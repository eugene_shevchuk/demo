package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"go.uber.org/zap"

	"crosspunks/app"
	"crosspunks/config"
	"crosspunks/repository/postgres"
	"crosspunks/server"
)

func main() {
	cfg := config.Get()

	logger, _ := zap.NewDevelopment()
	defer func() {
		_ = logger.Sync()
	}()

	db, err := postgres.Connect(cfg.DB)
	if err != nil {
		logger.Fatal("failed to connect db", zap.Error(err))
	}

	defer func() {
		_ = db.Close()
	}()

	punksRepo := postgres.NewPunksRepository(db)
	blockRepo := postgres.NewBlockRepository(db)

	app, err := app.NewApp(logger, cfg, punksRepo, blockRepo)
	if err != nil {
		logger.Fatal("failed to create app", zap.Error(err))
	}

	ctx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()

	app.Start(ctx)

	srv := server.NewServer(logger, cfg.Addr, app)
	srv.Start()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	<-sig
}
