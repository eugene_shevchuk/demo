package postgres

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
)

type BlockRepository struct {
	db *sqlx.DB
}

func NewBlockRepository(db *sqlx.DB) *BlockRepository {
	return &BlockRepository{db: db}
}

type block struct {
	Number    uint64    `db:"number"`
	UpdatedAt time.Time `db:"updated_at"`
}

func newBlock(number uint64) *block {
	return &block{
		Number: number,
	}
}

func (repo *BlockRepository) GetLastBlock(ctx context.Context) (uint64, error) {
	dto := &block{}
	query, args, err := repo.db.BindNamed(`
SELECT number,
       updated_at
FROM block
LIMIT 1;`, dto)
	if err != nil {
		return 0, err
	}
	if err = repo.db.GetContext(ctx, dto, query, args...); err != nil {
		return 0, err
	}
	return dto.Number, nil
}

func (repo *BlockRepository) Update(ctx context.Context, number uint64) (uint64, error) {
	dto := newBlock(number)
	query, args, err := repo.db.BindNamed(`
UPDATE block
SET number = :number
RETURNING updated_at;`, dto)
	if err != nil {
		return 0, err
	}
	if err := repo.db.GetContext(ctx, dto, query, args...); err != nil {
		return 0, err
	}
	return dto.Number, nil
}
