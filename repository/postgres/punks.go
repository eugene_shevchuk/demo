package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/jackc/pgtype"
	"github.com/jmoiron/sqlx"

	"crosspunks/types"
)

type PunksRepository struct {
	db *sqlx.DB
}

func NewPunksRepository(db *sqlx.DB) *PunksRepository {
	return &PunksRepository{db: db}
}

type punk struct {
	Index      int64            `db:"index"`
	Type       string           `db:"type"`
	Attributes pgtype.TextArray `db:"attributes"`
	Rank       int64            `db:"rank"`
	Owner      sql.NullString   `db:"owner"`
	IsForSale  bool             `db:"is_for_sale"`
	Seller     sql.NullString   `db:"seller"`
	MinValue   sql.NullString   `db:"min_value"`
	ToAddress  sql.NullString   `db:"to_address"`
	HasBid     bool             `db:"has_bid"`
	Bidder     sql.NullString   `db:"bidder"`
	Value      sql.NullString   `db:"value"`
	Block      int64            `db:"block_number"`
	CreatedAt  time.Time        `db:"created_at"`
	UpdatedAt  sql.NullTime     `db:"updated_at"`
}

func (p *punk) toModel() (*types.Punk, error) {
	out := &types.Punk{
		Idx:       p.Index,
		Type:      p.Type,
		Rank:      p.Rank,
		Block:     p.Block,
		CreatedAt: p.CreatedAt,
	}

	if err := p.Attributes.AssignTo(&out.Attributes); err != nil {
		return nil, err
	}

	if p.Owner.Valid {
		out.Owner = &p.Owner.String
	}

	if p.IsForSale {
		out.Offer = new(types.Offer)
		if p.Seller.Valid {
			out.Offer.Seller = p.Seller.String
		}
		if p.MinValue.Valid {
			out.Offer.MinValue = p.MinValue.String
		}
		if p.ToAddress.Valid {
			out.Offer.ToAddress = &p.ToAddress.String
		}
	}

	if p.HasBid {
		out.Bid = new(types.Bid)
		if p.Bidder.Valid {
			out.Bid.Bidder = p.Bidder.String
		}
		if p.Value.Valid {
			out.Bid.Value = p.Value.String
		}
	}

	if p.UpdatedAt.Valid {
		out.UpdatedAt = p.UpdatedAt.Time
	}

	return out, nil
}
func newPunk(in *types.Punk) (*punk, error) {
	out := &punk{
		Index: in.Idx,
		Block: in.Block,
	}
	out.Owner = NullStringFromStringPtr(in.Owner)
	if in.Offer != nil {
		out.IsForSale = true
		out.Seller = NullStringFromString(in.Offer.Seller)
		out.MinValue = NullStringFromString(in.Offer.MinValue)
		out.ToAddress = NullStringFromStringPtr(in.Offer.ToAddress)
	}
	if in.Bid != nil {
		out.HasBid = true
		out.Bidder = NullStringFromString(in.Bid.Bidder)
		out.Value = NullStringFromString(in.Bid.Value)
	}
	return out, nil
}

func (repo *PunksRepository) Update(ctx context.Context, in *types.Punk) (*types.Punk, error) {
	dto, err := newPunk(in)
	query, args, err := repo.db.BindNamed(`
UPDATE punks
SET owner        = :owner,
    is_for_sale  = :is_for_sale,
    seller       = :seller,
    min_value    = :min_value,
    to_address   = :to_address,
    has_bid      = :has_bid,
    bidder       = :bidder,
    value        = :value,
    block_number = :block_number,
    created_at   = now(),
    updated_at   = now()
WHERE index = :index
RETURNING type, attributes, rank, created_at, updated_at;`, dto)
	if err != nil {
		return nil, err
	}
	if err := repo.db.GetContext(ctx, dto, query, args...); err != nil {
		return nil, err
	}
	return dto.toModel()
}

func (repo *PunksRepository) GetByIdx(ctx context.Context, idx int64) (*types.Punk, error) {
	dto := &punk{Index: idx}
	query, args, err := repo.db.BindNamed(`
SELECT index,
       type,
       attributes,
       rank,
       owner,
       is_for_sale,
       seller,
       min_value,
       to_address,
       has_bid,
       bidder,
       value,
       block_number,
       created_at,
       updated_at
FROM punks
WHERE index = :index;`, dto)
	if err != nil {
		return nil, err
	}
	if err := repo.db.GetContext(ctx, dto, query, args...); err != nil {
		return nil, err
	}
	return dto.toModel()
}

func (repo *PunksRepository) filteredQuery(filter *types.PunksFilter) (string, []interface{}, error) {
	// language=SQL
	query := `
SELECT index,
       type,
       attributes,
       rank,
       owner,
       is_for_sale,
       seller,
       min_value,
       to_address,
       has_bid,
       bidder,
       value,
       block_number,
       created_at,
       updated_at
FROM punks`
	conditions := make([]Condition, 0)
	mapper := make(map[string]interface{})
	if len(filter.Types) > 0 {
		types := make([]interface{}, 0, len(filter.Types))
		for _, t := range filter.Types {
			types = append(types, t)
		}
		conditions = append(conditions, NewInCondition("type", types))
	}
	if len(filter.Attributes) > 0 {
		attributes := new(pgtype.TextArray)
		err := attributes.Set(filter.Attributes)
		if err != nil {
			return "", nil, err
		}
		conditions = append(conditions, NewArrayContainsCondition("attributes", attributes))
	}
	if filter.FromRank != nil {
		conditions = append(conditions, NewStrictMoreCondition("rank", *filter.FromRank))
	}
	if filter.ToRank != nil {
		conditions = append(conditions, NewStrictLessCondition("rank", *filter.ToRank))
	}
	if filter.Owner != nil {
		conditions = append(conditions, NewStrictCondition("owner", *filter.Owner))
	}
	if filter.IsForSale != nil {
		conditions = append(conditions, NewStrictCondition("is_for_sale", *filter.IsForSale))
	}
	if filter.Seller != nil {
		conditions = append(conditions, NewStrictCondition("seller", *filter.Seller))
	}
	if filter.HasBid != nil {
		conditions = append(conditions, NewStrictCondition("has_bid", *filter.HasBid))
	}
	if filter.Bidder != nil {
		conditions = append(conditions, NewStrictCondition("bidder", *filter.Bidder))
	}
	whereStr, mapper := PrepareConditions(conditions)
	if len(mapper) > 0 {
		query = fmt.Sprintf("%s WHERE %s", query, whereStr)
	}
	query, args, err := sqlx.Named(query, mapper)
	if err != nil {
		return "", nil, err
	}
	query, args, err = sqlx.In(query, args...)
	if err != nil {
		return "", nil, err
	}
	return repo.db.Rebind(query), args, nil
}

func (repo *PunksRepository) GetPunks(ctx context.Context, filter *types.PunksFilter) ([]*types.Punk, error) {
	query, args, err := repo.filteredQuery(filter)
	if err != nil {
		return nil, err
	}
	query = fmt.Sprintf("%s ORDER BY index LIMIT %d OFFSET %d;", query, filter.Limit, filter.Offset)
	var punks []*punk
	err = repo.db.SelectContext(ctx, &punks, query, args...)
	if err != nil {
		return nil, err
	}
	out := make([]*types.Punk, 0, len(punks))
	for _, punk := range punks {
		p, err := punk.toModel()
		if err != nil {
			return nil, err
		}
		out = append(out, p)
	}
	return out, nil
}

func (repo *PunksRepository) Count(ctx context.Context, filter *types.PunksFilter) (int, error) {
	var count int
	query, args, err := repo.filteredQuery(filter)
	if err != nil {
		return 0, err
	}
	query = fmt.Sprintf(`
SELECT COUNT(*) 
FROM (%s) AS pks;`, query)
	err = repo.db.GetContext(ctx, &count, query, args...)
	if err != nil {
		return 0, err
	}
	return count, nil
}
