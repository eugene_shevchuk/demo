CREATE TABLE IF NOT EXISTS block
(
    number     integer                  NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

INSERT INTO block (number, updated_at)
VALUES (10673850, now());