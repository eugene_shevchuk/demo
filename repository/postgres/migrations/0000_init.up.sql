CREATE TABLE IF NOT EXISTS punks
(
    index        INTEGER                  NOT NULL
        CONSTRAINT punks_pk PRIMARY KEY,
    type         VARCHAR(20)              NOT NULL,
    attributes   text[]                   NOT NULL,
    rank         INTEGER                  NOT NULL,
    owner        VARCHAR(42),
    -- offer
    is_for_sale  BOOLEAN                  NOT NULL DEFAULT false,
    seller       VARCHAR(42),
    min_value    NUMERIC,
    to_address   VARCHAR(42),
    -- bid
    has_bid      BOOLEAN                  NOT NULL DEFAULT false,
    bidder       VARCHAR(42),
    value        NUMERIC,
    --
    block_number INTEGER                  NOT NULL DEFAULT 0,
    created_at   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    updated_at   TIMESTAMP WITH TIME ZONE
);