FROM golang:1.17-alpine as build

RUN apk --update --no-cache add build-base

WORKDIR  $GOPATH/src/crosspunks

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go build -o /go/bin/crosspunks .

FROM alpine:3.14
RUN apk --update --no-cache add ca-certificates tzdata
COPY --from=build /go/bin/crosspunks /bin/
EXPOSE 8080 8080

ENTRYPOINT ["crosspunks"]