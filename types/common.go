package types

import (
	"errors"

	"github.com/ethereum/go-ethereum/common"
)

var (
	ErrNoObject = errors.New("no object")
)

var NilAddress = common.Address{}

type Page struct {
	// in:query
	Limit uint64 `json:"limit" schema:"limit"`
	// in:query
	Offset uint64 `json:"offset" schema:"offset"`
}
