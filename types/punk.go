package types

import (
	"time"
)

type Punk struct {
	Idx        int64     `json:"idx"`
	Type       string    `json:"type"`
	Attributes []string  `json:"attributes"`
	Rank       int64     `json:"rank"`
	Owner      *string   `json:"owner,omitempty"`
	Offer      *Offer    `json:"offer,omitempty"`
	Bid        *Bid      `json:"bid,omitempty"`
	Block      int64     `json:"-"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}

type Offer struct {
	Seller    string  `json:"seller"`
	MinValue  string  `json:"min_value"`
	ToAddress *string `json:"to_address,omitempty"`
}

type Bid struct {
	Bidder string `json:"bidder"`
	Value  string `json:"value"`
}

type PunksResponse struct {
	Punks []*Punk `json:"punks"`
	Total int     `json:"total"`
}

// swagger:parameters punks
// nolint
type PunksFilter struct {
	// in:query
	Types []string `json:"types,omitempty" schema:"types"`
	// in:query
	Attributes []string `json:"attributes,omitempty" schema:"attributes"`
	// in:query
	FromRank *uint64 `json:"from_rank,omitesmpty" schema:"from_rank"`
	// in:query
	ToRank *uint64 `json:"to_rank,omitempty"  schema:"to_rank"`
	// in:query
	// example: "0x36894d06ac91B09760b4310C75Ed2348E3eA063C"
	Owner *string `json:"owner,omitempty" schema:"owner"`
	// in:query
	// enum: true, false, 1, 0
	IsForSale *bool `json:"is_for_sale,omitempty" schema:"is_for_sale"`
	// in:query
	// example: "0x36894d06ac91B09760b4310C75Ed2348E3eA063C"
	Seller *string `json:"seller,omitempty" schema:"seller"`
	// in:query
	// enum: true, false, 1, 0
	HasBid *bool `json:"has_bid,omitempty" schema:"has_bid"`
	// in:query
	// example: "0x36894d06ac91B09760b4310C75Ed2348E3eA063C"
	Bidder *string `json:"bidder,omitempty" schema:"bidder"`
	Page
}
