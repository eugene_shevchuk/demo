// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package web3

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// CrossPunksMetaData contains all meta data concerning the CrossPunks contract.
var CrossPunksMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"string\",\"name\":\"name\",\"type\":\"string\"},{\"internalType\":\"string\",\"name\":\"symbol\",\"type\":\"string\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"approved\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bool\",\"name\":\"approved\",\"type\":\"bool\"}],\"name\":\"ApprovalForAll\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"maskIndex\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"newName\",\"type\":\"string\"}],\"name\":\"NameChange\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"CROSSPUNKS_PROVENANCE\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"MAX_NFT_SUPPLY\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"airDropId\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes4\",\"name\":\"interfaceId\",\"type\":\"bytes4\"}],\"name\":\"supportsInterface\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"usersAirdrop\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"isExists\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"id\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"referralBuyIndex\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"usersAirdropAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"winners\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"startAirDrop\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address[]\",\"name\":\"users\",\"type\":\"address[]\"},{\"internalType\":\"uint256\",\"name\":\"_column\",\"type\":\"uint256\"}],\"name\":\"initializeOwners\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"finishInitilizeOwners\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"ownerOf\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"tokenURI\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"baseURI\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"tokenOfOwnerByIndex\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"index\",\"type\":\"uint256\"}],\"name\":\"tokenByIndex\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[],\"name\":\"getNFTPrice\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"numberOfNfts\",\"type\":\"uint256\"}],\"name\":\"mintNFT\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\",\"payable\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"numberOfNfts\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_airDropId\",\"type\":\"uint256\"}],\"name\":\"mintNFTAirDrop\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\",\"payable\":true},{\"inputs\":[],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"getApproved\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"approved\",\"type\":\"bool\"}],\"name\":\"setApprovalForAll\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"}],\"name\":\"isApprovedForAll\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"safeTransferFrom\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// CrossPunksABI is the input ABI used to generate the binding from.
// Deprecated: Use CrossPunksMetaData.ABI instead.
var CrossPunksABI = CrossPunksMetaData.ABI

// CrossPunks is an auto generated Go binding around an Ethereum contract.
type CrossPunks struct {
	CrossPunksCaller     // Read-only binding to the contract
	CrossPunksTransactor // Write-only binding to the contract
	CrossPunksFilterer   // Log filterer for contract events
}

// CrossPunksCaller is an auto generated read-only Go binding around an Ethereum contract.
type CrossPunksCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// CrossPunksTransactor is an auto generated write-only Go binding around an Ethereum contract.
type CrossPunksTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// CrossPunksFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type CrossPunksFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// CrossPunksSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type CrossPunksSession struct {
	Contract     *CrossPunks       // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// CrossPunksCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type CrossPunksCallerSession struct {
	Contract *CrossPunksCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts     // Call options to use throughout this session
}

// CrossPunksTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type CrossPunksTransactorSession struct {
	Contract     *CrossPunksTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts     // Transaction auth options to use throughout this session
}

// CrossPunksRaw is an auto generated low-level Go binding around an Ethereum contract.
type CrossPunksRaw struct {
	Contract *CrossPunks // Generic contract binding to access the raw methods on
}

// CrossPunksCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type CrossPunksCallerRaw struct {
	Contract *CrossPunksCaller // Generic read-only contract binding to access the raw methods on
}

// CrossPunksTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type CrossPunksTransactorRaw struct {
	Contract *CrossPunksTransactor // Generic write-only contract binding to access the raw methods on
}

// NewCrossPunks creates a new instance of CrossPunks, bound to a specific deployed contract.
func NewCrossPunks(address common.Address, backend bind.ContractBackend) (*CrossPunks, error) {
	contract, err := bindCrossPunks(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &CrossPunks{CrossPunksCaller: CrossPunksCaller{contract: contract}, CrossPunksTransactor: CrossPunksTransactor{contract: contract}, CrossPunksFilterer: CrossPunksFilterer{contract: contract}}, nil
}

// NewCrossPunksCaller creates a new read-only instance of CrossPunks, bound to a specific deployed contract.
func NewCrossPunksCaller(address common.Address, caller bind.ContractCaller) (*CrossPunksCaller, error) {
	contract, err := bindCrossPunks(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &CrossPunksCaller{contract: contract}, nil
}

// NewCrossPunksTransactor creates a new write-only instance of CrossPunks, bound to a specific deployed contract.
func NewCrossPunksTransactor(address common.Address, transactor bind.ContractTransactor) (*CrossPunksTransactor, error) {
	contract, err := bindCrossPunks(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &CrossPunksTransactor{contract: contract}, nil
}

// NewCrossPunksFilterer creates a new log filterer instance of CrossPunks, bound to a specific deployed contract.
func NewCrossPunksFilterer(address common.Address, filterer bind.ContractFilterer) (*CrossPunksFilterer, error) {
	contract, err := bindCrossPunks(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &CrossPunksFilterer{contract: contract}, nil
}

// bindCrossPunks binds a generic wrapper to an already deployed contract.
func bindCrossPunks(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(CrossPunksABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_CrossPunks *CrossPunksRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _CrossPunks.Contract.CrossPunksCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_CrossPunks *CrossPunksRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _CrossPunks.Contract.CrossPunksTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_CrossPunks *CrossPunksRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _CrossPunks.Contract.CrossPunksTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_CrossPunks *CrossPunksCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _CrossPunks.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_CrossPunks *CrossPunksTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _CrossPunks.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_CrossPunks *CrossPunksTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _CrossPunks.Contract.contract.Transact(opts, method, params...)
}

// CROSSPUNKSPROVENANCE is a free data retrieval call binding the contract method 0x0c1d62fc.
//
// Solidity: function CROSSPUNKS_PROVENANCE() view returns(string)
func (_CrossPunks *CrossPunksCaller) CROSSPUNKSPROVENANCE(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "CROSSPUNKS_PROVENANCE")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// CROSSPUNKSPROVENANCE is a free data retrieval call binding the contract method 0x0c1d62fc.
//
// Solidity: function CROSSPUNKS_PROVENANCE() view returns(string)
func (_CrossPunks *CrossPunksSession) CROSSPUNKSPROVENANCE() (string, error) {
	return _CrossPunks.Contract.CROSSPUNKSPROVENANCE(&_CrossPunks.CallOpts)
}

// CROSSPUNKSPROVENANCE is a free data retrieval call binding the contract method 0x0c1d62fc.
//
// Solidity: function CROSSPUNKS_PROVENANCE() view returns(string)
func (_CrossPunks *CrossPunksCallerSession) CROSSPUNKSPROVENANCE() (string, error) {
	return _CrossPunks.Contract.CROSSPUNKSPROVENANCE(&_CrossPunks.CallOpts)
}

// MAXNFTSUPPLY is a free data retrieval call binding the contract method 0xb5077f44.
//
// Solidity: function MAX_NFT_SUPPLY() view returns(uint256)
func (_CrossPunks *CrossPunksCaller) MAXNFTSUPPLY(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "MAX_NFT_SUPPLY")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// MAXNFTSUPPLY is a free data retrieval call binding the contract method 0xb5077f44.
//
// Solidity: function MAX_NFT_SUPPLY() view returns(uint256)
func (_CrossPunks *CrossPunksSession) MAXNFTSUPPLY() (*big.Int, error) {
	return _CrossPunks.Contract.MAXNFTSUPPLY(&_CrossPunks.CallOpts)
}

// MAXNFTSUPPLY is a free data retrieval call binding the contract method 0xb5077f44.
//
// Solidity: function MAX_NFT_SUPPLY() view returns(uint256)
func (_CrossPunks *CrossPunksCallerSession) MAXNFTSUPPLY() (*big.Int, error) {
	return _CrossPunks.Contract.MAXNFTSUPPLY(&_CrossPunks.CallOpts)
}

// AirDropId is a free data retrieval call binding the contract method 0x9c8dff86.
//
// Solidity: function airDropId() view returns(uint256)
func (_CrossPunks *CrossPunksCaller) AirDropId(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "airDropId")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// AirDropId is a free data retrieval call binding the contract method 0x9c8dff86.
//
// Solidity: function airDropId() view returns(uint256)
func (_CrossPunks *CrossPunksSession) AirDropId() (*big.Int, error) {
	return _CrossPunks.Contract.AirDropId(&_CrossPunks.CallOpts)
}

// AirDropId is a free data retrieval call binding the contract method 0x9c8dff86.
//
// Solidity: function airDropId() view returns(uint256)
func (_CrossPunks *CrossPunksCallerSession) AirDropId() (*big.Int, error) {
	return _CrossPunks.Contract.AirDropId(&_CrossPunks.CallOpts)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_CrossPunks *CrossPunksCaller) BalanceOf(opts *bind.CallOpts, owner common.Address) (*big.Int, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "balanceOf", owner)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_CrossPunks *CrossPunksSession) BalanceOf(owner common.Address) (*big.Int, error) {
	return _CrossPunks.Contract.BalanceOf(&_CrossPunks.CallOpts, owner)
}

// BalanceOf is a free data retrieval call binding the contract method 0x70a08231.
//
// Solidity: function balanceOf(address owner) view returns(uint256)
func (_CrossPunks *CrossPunksCallerSession) BalanceOf(owner common.Address) (*big.Int, error) {
	return _CrossPunks.Contract.BalanceOf(&_CrossPunks.CallOpts, owner)
}

// BaseURI is a free data retrieval call binding the contract method 0x6c0360eb.
//
// Solidity: function baseURI() view returns(string)
func (_CrossPunks *CrossPunksCaller) BaseURI(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "baseURI")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// BaseURI is a free data retrieval call binding the contract method 0x6c0360eb.
//
// Solidity: function baseURI() view returns(string)
func (_CrossPunks *CrossPunksSession) BaseURI() (string, error) {
	return _CrossPunks.Contract.BaseURI(&_CrossPunks.CallOpts)
}

// BaseURI is a free data retrieval call binding the contract method 0x6c0360eb.
//
// Solidity: function baseURI() view returns(string)
func (_CrossPunks *CrossPunksCallerSession) BaseURI() (string, error) {
	return _CrossPunks.Contract.BaseURI(&_CrossPunks.CallOpts)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_CrossPunks *CrossPunksCaller) GetApproved(opts *bind.CallOpts, tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "getApproved", tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_CrossPunks *CrossPunksSession) GetApproved(tokenId *big.Int) (common.Address, error) {
	return _CrossPunks.Contract.GetApproved(&_CrossPunks.CallOpts, tokenId)
}

// GetApproved is a free data retrieval call binding the contract method 0x081812fc.
//
// Solidity: function getApproved(uint256 tokenId) view returns(address)
func (_CrossPunks *CrossPunksCallerSession) GetApproved(tokenId *big.Int) (common.Address, error) {
	return _CrossPunks.Contract.GetApproved(&_CrossPunks.CallOpts, tokenId)
}

// GetNFTPrice is a free data retrieval call binding the contract method 0xfb107a4f.
//
// Solidity: function getNFTPrice() view returns(uint256)
func (_CrossPunks *CrossPunksCaller) GetNFTPrice(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "getNFTPrice")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// GetNFTPrice is a free data retrieval call binding the contract method 0xfb107a4f.
//
// Solidity: function getNFTPrice() view returns(uint256)
func (_CrossPunks *CrossPunksSession) GetNFTPrice() (*big.Int, error) {
	return _CrossPunks.Contract.GetNFTPrice(&_CrossPunks.CallOpts)
}

// GetNFTPrice is a free data retrieval call binding the contract method 0xfb107a4f.
//
// Solidity: function getNFTPrice() view returns(uint256)
func (_CrossPunks *CrossPunksCallerSession) GetNFTPrice() (*big.Int, error) {
	return _CrossPunks.Contract.GetNFTPrice(&_CrossPunks.CallOpts)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_CrossPunks *CrossPunksCaller) IsApprovedForAll(opts *bind.CallOpts, owner common.Address, operator common.Address) (bool, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "isApprovedForAll", owner, operator)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_CrossPunks *CrossPunksSession) IsApprovedForAll(owner common.Address, operator common.Address) (bool, error) {
	return _CrossPunks.Contract.IsApprovedForAll(&_CrossPunks.CallOpts, owner, operator)
}

// IsApprovedForAll is a free data retrieval call binding the contract method 0xe985e9c5.
//
// Solidity: function isApprovedForAll(address owner, address operator) view returns(bool)
func (_CrossPunks *CrossPunksCallerSession) IsApprovedForAll(owner common.Address, operator common.Address) (bool, error) {
	return _CrossPunks.Contract.IsApprovedForAll(&_CrossPunks.CallOpts, owner, operator)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_CrossPunks *CrossPunksCaller) Name(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "name")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_CrossPunks *CrossPunksSession) Name() (string, error) {
	return _CrossPunks.Contract.Name(&_CrossPunks.CallOpts)
}

// Name is a free data retrieval call binding the contract method 0x06fdde03.
//
// Solidity: function name() view returns(string)
func (_CrossPunks *CrossPunksCallerSession) Name() (string, error) {
	return _CrossPunks.Contract.Name(&_CrossPunks.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_CrossPunks *CrossPunksCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_CrossPunks *CrossPunksSession) Owner() (common.Address, error) {
	return _CrossPunks.Contract.Owner(&_CrossPunks.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_CrossPunks *CrossPunksCallerSession) Owner() (common.Address, error) {
	return _CrossPunks.Contract.Owner(&_CrossPunks.CallOpts)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_CrossPunks *CrossPunksCaller) OwnerOf(opts *bind.CallOpts, tokenId *big.Int) (common.Address, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "ownerOf", tokenId)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_CrossPunks *CrossPunksSession) OwnerOf(tokenId *big.Int) (common.Address, error) {
	return _CrossPunks.Contract.OwnerOf(&_CrossPunks.CallOpts, tokenId)
}

// OwnerOf is a free data retrieval call binding the contract method 0x6352211e.
//
// Solidity: function ownerOf(uint256 tokenId) view returns(address)
func (_CrossPunks *CrossPunksCallerSession) OwnerOf(tokenId *big.Int) (common.Address, error) {
	return _CrossPunks.Contract.OwnerOf(&_CrossPunks.CallOpts, tokenId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_CrossPunks *CrossPunksCaller) SupportsInterface(opts *bind.CallOpts, interfaceId [4]byte) (bool, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "supportsInterface", interfaceId)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_CrossPunks *CrossPunksSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _CrossPunks.Contract.SupportsInterface(&_CrossPunks.CallOpts, interfaceId)
}

// SupportsInterface is a free data retrieval call binding the contract method 0x01ffc9a7.
//
// Solidity: function supportsInterface(bytes4 interfaceId) view returns(bool)
func (_CrossPunks *CrossPunksCallerSession) SupportsInterface(interfaceId [4]byte) (bool, error) {
	return _CrossPunks.Contract.SupportsInterface(&_CrossPunks.CallOpts, interfaceId)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_CrossPunks *CrossPunksCaller) Symbol(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "symbol")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_CrossPunks *CrossPunksSession) Symbol() (string, error) {
	return _CrossPunks.Contract.Symbol(&_CrossPunks.CallOpts)
}

// Symbol is a free data retrieval call binding the contract method 0x95d89b41.
//
// Solidity: function symbol() view returns(string)
func (_CrossPunks *CrossPunksCallerSession) Symbol() (string, error) {
	return _CrossPunks.Contract.Symbol(&_CrossPunks.CallOpts)
}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_CrossPunks *CrossPunksCaller) TokenByIndex(opts *bind.CallOpts, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "tokenByIndex", index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_CrossPunks *CrossPunksSession) TokenByIndex(index *big.Int) (*big.Int, error) {
	return _CrossPunks.Contract.TokenByIndex(&_CrossPunks.CallOpts, index)
}

// TokenByIndex is a free data retrieval call binding the contract method 0x4f6ccce7.
//
// Solidity: function tokenByIndex(uint256 index) view returns(uint256)
func (_CrossPunks *CrossPunksCallerSession) TokenByIndex(index *big.Int) (*big.Int, error) {
	return _CrossPunks.Contract.TokenByIndex(&_CrossPunks.CallOpts, index)
}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_CrossPunks *CrossPunksCaller) TokenOfOwnerByIndex(opts *bind.CallOpts, owner common.Address, index *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "tokenOfOwnerByIndex", owner, index)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_CrossPunks *CrossPunksSession) TokenOfOwnerByIndex(owner common.Address, index *big.Int) (*big.Int, error) {
	return _CrossPunks.Contract.TokenOfOwnerByIndex(&_CrossPunks.CallOpts, owner, index)
}

// TokenOfOwnerByIndex is a free data retrieval call binding the contract method 0x2f745c59.
//
// Solidity: function tokenOfOwnerByIndex(address owner, uint256 index) view returns(uint256)
func (_CrossPunks *CrossPunksCallerSession) TokenOfOwnerByIndex(owner common.Address, index *big.Int) (*big.Int, error) {
	return _CrossPunks.Contract.TokenOfOwnerByIndex(&_CrossPunks.CallOpts, owner, index)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_CrossPunks *CrossPunksCaller) TokenURI(opts *bind.CallOpts, tokenId *big.Int) (string, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "tokenURI", tokenId)

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_CrossPunks *CrossPunksSession) TokenURI(tokenId *big.Int) (string, error) {
	return _CrossPunks.Contract.TokenURI(&_CrossPunks.CallOpts, tokenId)
}

// TokenURI is a free data retrieval call binding the contract method 0xc87b56dd.
//
// Solidity: function tokenURI(uint256 tokenId) view returns(string)
func (_CrossPunks *CrossPunksCallerSession) TokenURI(tokenId *big.Int) (string, error) {
	return _CrossPunks.Contract.TokenURI(&_CrossPunks.CallOpts, tokenId)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_CrossPunks *CrossPunksCaller) TotalSupply(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "totalSupply")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_CrossPunks *CrossPunksSession) TotalSupply() (*big.Int, error) {
	return _CrossPunks.Contract.TotalSupply(&_CrossPunks.CallOpts)
}

// TotalSupply is a free data retrieval call binding the contract method 0x18160ddd.
//
// Solidity: function totalSupply() view returns(uint256)
func (_CrossPunks *CrossPunksCallerSession) TotalSupply() (*big.Int, error) {
	return _CrossPunks.Contract.TotalSupply(&_CrossPunks.CallOpts)
}

// UsersAirdrop is a free data retrieval call binding the contract method 0xcf44f3e5.
//
// Solidity: function usersAirdrop(address ) view returns(bool isExists, uint256 id, uint256 referralBuyIndex)
func (_CrossPunks *CrossPunksCaller) UsersAirdrop(opts *bind.CallOpts, arg0 common.Address) (struct {
	IsExists         bool
	Id               *big.Int
	ReferralBuyIndex *big.Int
}, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "usersAirdrop", arg0)

	outstruct := new(struct {
		IsExists         bool
		Id               *big.Int
		ReferralBuyIndex *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.IsExists = *abi.ConvertType(out[0], new(bool)).(*bool)
	outstruct.Id = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.ReferralBuyIndex = *abi.ConvertType(out[2], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// UsersAirdrop is a free data retrieval call binding the contract method 0xcf44f3e5.
//
// Solidity: function usersAirdrop(address ) view returns(bool isExists, uint256 id, uint256 referralBuyIndex)
func (_CrossPunks *CrossPunksSession) UsersAirdrop(arg0 common.Address) (struct {
	IsExists         bool
	Id               *big.Int
	ReferralBuyIndex *big.Int
}, error) {
	return _CrossPunks.Contract.UsersAirdrop(&_CrossPunks.CallOpts, arg0)
}

// UsersAirdrop is a free data retrieval call binding the contract method 0xcf44f3e5.
//
// Solidity: function usersAirdrop(address ) view returns(bool isExists, uint256 id, uint256 referralBuyIndex)
func (_CrossPunks *CrossPunksCallerSession) UsersAirdrop(arg0 common.Address) (struct {
	IsExists         bool
	Id               *big.Int
	ReferralBuyIndex *big.Int
}, error) {
	return _CrossPunks.Contract.UsersAirdrop(&_CrossPunks.CallOpts, arg0)
}

// UsersAirdropAddress is a free data retrieval call binding the contract method 0xc9ea86d1.
//
// Solidity: function usersAirdropAddress(uint256 ) view returns(address)
func (_CrossPunks *CrossPunksCaller) UsersAirdropAddress(opts *bind.CallOpts, arg0 *big.Int) (common.Address, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "usersAirdropAddress", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// UsersAirdropAddress is a free data retrieval call binding the contract method 0xc9ea86d1.
//
// Solidity: function usersAirdropAddress(uint256 ) view returns(address)
func (_CrossPunks *CrossPunksSession) UsersAirdropAddress(arg0 *big.Int) (common.Address, error) {
	return _CrossPunks.Contract.UsersAirdropAddress(&_CrossPunks.CallOpts, arg0)
}

// UsersAirdropAddress is a free data retrieval call binding the contract method 0xc9ea86d1.
//
// Solidity: function usersAirdropAddress(uint256 ) view returns(address)
func (_CrossPunks *CrossPunksCallerSession) UsersAirdropAddress(arg0 *big.Int) (common.Address, error) {
	return _CrossPunks.Contract.UsersAirdropAddress(&_CrossPunks.CallOpts, arg0)
}

// Winners is a free data retrieval call binding the contract method 0xa2fb1175.
//
// Solidity: function winners(uint256 ) view returns(address)
func (_CrossPunks *CrossPunksCaller) Winners(opts *bind.CallOpts, arg0 *big.Int) (common.Address, error) {
	var out []interface{}
	err := _CrossPunks.contract.Call(opts, &out, "winners", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Winners is a free data retrieval call binding the contract method 0xa2fb1175.
//
// Solidity: function winners(uint256 ) view returns(address)
func (_CrossPunks *CrossPunksSession) Winners(arg0 *big.Int) (common.Address, error) {
	return _CrossPunks.Contract.Winners(&_CrossPunks.CallOpts, arg0)
}

// Winners is a free data retrieval call binding the contract method 0xa2fb1175.
//
// Solidity: function winners(uint256 ) view returns(address)
func (_CrossPunks *CrossPunksCallerSession) Winners(arg0 *big.Int) (common.Address, error) {
	return _CrossPunks.Contract.Winners(&_CrossPunks.CallOpts, arg0)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_CrossPunks *CrossPunksTransactor) Approve(opts *bind.TransactOpts, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "approve", to, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_CrossPunks *CrossPunksSession) Approve(to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.Approve(&_CrossPunks.TransactOpts, to, tokenId)
}

// Approve is a paid mutator transaction binding the contract method 0x095ea7b3.
//
// Solidity: function approve(address to, uint256 tokenId) returns()
func (_CrossPunks *CrossPunksTransactorSession) Approve(to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.Approve(&_CrossPunks.TransactOpts, to, tokenId)
}

// FinishInitilizeOwners is a paid mutator transaction binding the contract method 0xb2cacca1.
//
// Solidity: function finishInitilizeOwners() returns()
func (_CrossPunks *CrossPunksTransactor) FinishInitilizeOwners(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "finishInitilizeOwners")
}

// FinishInitilizeOwners is a paid mutator transaction binding the contract method 0xb2cacca1.
//
// Solidity: function finishInitilizeOwners() returns()
func (_CrossPunks *CrossPunksSession) FinishInitilizeOwners() (*types.Transaction, error) {
	return _CrossPunks.Contract.FinishInitilizeOwners(&_CrossPunks.TransactOpts)
}

// FinishInitilizeOwners is a paid mutator transaction binding the contract method 0xb2cacca1.
//
// Solidity: function finishInitilizeOwners() returns()
func (_CrossPunks *CrossPunksTransactorSession) FinishInitilizeOwners() (*types.Transaction, error) {
	return _CrossPunks.Contract.FinishInitilizeOwners(&_CrossPunks.TransactOpts)
}

// InitializeOwners is a paid mutator transaction binding the contract method 0x1b23c4fe.
//
// Solidity: function initializeOwners(address[] users, uint256 _column) returns()
func (_CrossPunks *CrossPunksTransactor) InitializeOwners(opts *bind.TransactOpts, users []common.Address, _column *big.Int) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "initializeOwners", users, _column)
}

// InitializeOwners is a paid mutator transaction binding the contract method 0x1b23c4fe.
//
// Solidity: function initializeOwners(address[] users, uint256 _column) returns()
func (_CrossPunks *CrossPunksSession) InitializeOwners(users []common.Address, _column *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.InitializeOwners(&_CrossPunks.TransactOpts, users, _column)
}

// InitializeOwners is a paid mutator transaction binding the contract method 0x1b23c4fe.
//
// Solidity: function initializeOwners(address[] users, uint256 _column) returns()
func (_CrossPunks *CrossPunksTransactorSession) InitializeOwners(users []common.Address, _column *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.InitializeOwners(&_CrossPunks.TransactOpts, users, _column)
}

// MintNFT is a paid mutator transaction binding the contract method 0x92642744.
//
// Solidity: function mintNFT(uint256 numberOfNfts) payable returns()
func (_CrossPunks *CrossPunksTransactor) MintNFT(opts *bind.TransactOpts, numberOfNfts *big.Int) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "mintNFT", numberOfNfts)
}

// MintNFT is a paid mutator transaction binding the contract method 0x92642744.
//
// Solidity: function mintNFT(uint256 numberOfNfts) payable returns()
func (_CrossPunks *CrossPunksSession) MintNFT(numberOfNfts *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.MintNFT(&_CrossPunks.TransactOpts, numberOfNfts)
}

// MintNFT is a paid mutator transaction binding the contract method 0x92642744.
//
// Solidity: function mintNFT(uint256 numberOfNfts) payable returns()
func (_CrossPunks *CrossPunksTransactorSession) MintNFT(numberOfNfts *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.MintNFT(&_CrossPunks.TransactOpts, numberOfNfts)
}

// MintNFTAirDrop is a paid mutator transaction binding the contract method 0xf00e6f14.
//
// Solidity: function mintNFTAirDrop(uint256 numberOfNfts, uint256 _airDropId) payable returns()
func (_CrossPunks *CrossPunksTransactor) MintNFTAirDrop(opts *bind.TransactOpts, numberOfNfts *big.Int, _airDropId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "mintNFTAirDrop", numberOfNfts, _airDropId)
}

// MintNFTAirDrop is a paid mutator transaction binding the contract method 0xf00e6f14.
//
// Solidity: function mintNFTAirDrop(uint256 numberOfNfts, uint256 _airDropId) payable returns()
func (_CrossPunks *CrossPunksSession) MintNFTAirDrop(numberOfNfts *big.Int, _airDropId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.MintNFTAirDrop(&_CrossPunks.TransactOpts, numberOfNfts, _airDropId)
}

// MintNFTAirDrop is a paid mutator transaction binding the contract method 0xf00e6f14.
//
// Solidity: function mintNFTAirDrop(uint256 numberOfNfts, uint256 _airDropId) payable returns()
func (_CrossPunks *CrossPunksTransactorSession) MintNFTAirDrop(numberOfNfts *big.Int, _airDropId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.MintNFTAirDrop(&_CrossPunks.TransactOpts, numberOfNfts, _airDropId)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_CrossPunks *CrossPunksTransactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_CrossPunks *CrossPunksSession) RenounceOwnership() (*types.Transaction, error) {
	return _CrossPunks.Contract.RenounceOwnership(&_CrossPunks.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_CrossPunks *CrossPunksTransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _CrossPunks.Contract.RenounceOwnership(&_CrossPunks.TransactOpts)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_CrossPunks *CrossPunksTransactor) SafeTransferFrom(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "safeTransferFrom", from, to, tokenId, _data)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_CrossPunks *CrossPunksSession) SafeTransferFrom(from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _CrossPunks.Contract.SafeTransferFrom(&_CrossPunks.TransactOpts, from, to, tokenId, _data)
}

// SafeTransferFrom is a paid mutator transaction binding the contract method 0xb88d4fde.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId, bytes _data) returns()
func (_CrossPunks *CrossPunksTransactorSession) SafeTransferFrom(from common.Address, to common.Address, tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _CrossPunks.Contract.SafeTransferFrom(&_CrossPunks.TransactOpts, from, to, tokenId, _data)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_CrossPunks *CrossPunksTransactor) SafeTransferFrom0(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "safeTransferFrom0", from, to, tokenId)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_CrossPunks *CrossPunksSession) SafeTransferFrom0(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.SafeTransferFrom0(&_CrossPunks.TransactOpts, from, to, tokenId)
}

// SafeTransferFrom0 is a paid mutator transaction binding the contract method 0x42842e0e.
//
// Solidity: function safeTransferFrom(address from, address to, uint256 tokenId) returns()
func (_CrossPunks *CrossPunksTransactorSession) SafeTransferFrom0(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.SafeTransferFrom0(&_CrossPunks.TransactOpts, from, to, tokenId)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_CrossPunks *CrossPunksTransactor) SetApprovalForAll(opts *bind.TransactOpts, operator common.Address, approved bool) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "setApprovalForAll", operator, approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_CrossPunks *CrossPunksSession) SetApprovalForAll(operator common.Address, approved bool) (*types.Transaction, error) {
	return _CrossPunks.Contract.SetApprovalForAll(&_CrossPunks.TransactOpts, operator, approved)
}

// SetApprovalForAll is a paid mutator transaction binding the contract method 0xa22cb465.
//
// Solidity: function setApprovalForAll(address operator, bool approved) returns()
func (_CrossPunks *CrossPunksTransactorSession) SetApprovalForAll(operator common.Address, approved bool) (*types.Transaction, error) {
	return _CrossPunks.Contract.SetApprovalForAll(&_CrossPunks.TransactOpts, operator, approved)
}

// StartAirDrop is a paid mutator transaction binding the contract method 0x283a1e16.
//
// Solidity: function startAirDrop() returns(uint256)
func (_CrossPunks *CrossPunksTransactor) StartAirDrop(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "startAirDrop")
}

// StartAirDrop is a paid mutator transaction binding the contract method 0x283a1e16.
//
// Solidity: function startAirDrop() returns(uint256)
func (_CrossPunks *CrossPunksSession) StartAirDrop() (*types.Transaction, error) {
	return _CrossPunks.Contract.StartAirDrop(&_CrossPunks.TransactOpts)
}

// StartAirDrop is a paid mutator transaction binding the contract method 0x283a1e16.
//
// Solidity: function startAirDrop() returns(uint256)
func (_CrossPunks *CrossPunksTransactorSession) StartAirDrop() (*types.Transaction, error) {
	return _CrossPunks.Contract.StartAirDrop(&_CrossPunks.TransactOpts)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_CrossPunks *CrossPunksTransactor) TransferFrom(opts *bind.TransactOpts, from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "transferFrom", from, to, tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_CrossPunks *CrossPunksSession) TransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.TransferFrom(&_CrossPunks.TransactOpts, from, to, tokenId)
}

// TransferFrom is a paid mutator transaction binding the contract method 0x23b872dd.
//
// Solidity: function transferFrom(address from, address to, uint256 tokenId) returns()
func (_CrossPunks *CrossPunksTransactorSession) TransferFrom(from common.Address, to common.Address, tokenId *big.Int) (*types.Transaction, error) {
	return _CrossPunks.Contract.TransferFrom(&_CrossPunks.TransactOpts, from, to, tokenId)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_CrossPunks *CrossPunksTransactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_CrossPunks *CrossPunksSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _CrossPunks.Contract.TransferOwnership(&_CrossPunks.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_CrossPunks *CrossPunksTransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _CrossPunks.Contract.TransferOwnership(&_CrossPunks.TransactOpts, newOwner)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_CrossPunks *CrossPunksTransactor) Withdraw(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _CrossPunks.contract.Transact(opts, "withdraw")
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_CrossPunks *CrossPunksSession) Withdraw() (*types.Transaction, error) {
	return _CrossPunks.Contract.Withdraw(&_CrossPunks.TransactOpts)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns()
func (_CrossPunks *CrossPunksTransactorSession) Withdraw() (*types.Transaction, error) {
	return _CrossPunks.Contract.Withdraw(&_CrossPunks.TransactOpts)
}

// CrossPunksApprovalIterator is returned from FilterApproval and is used to iterate over the raw logs and unpacked data for Approval events raised by the CrossPunks contract.
type CrossPunksApprovalIterator struct {
	Event *CrossPunksApproval // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksApprovalIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksApproval)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksApproval)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksApprovalIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksApprovalIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksApproval represents a Approval event raised by the CrossPunks contract.
type CrossPunksApproval struct {
	Owner    common.Address
	Approved common.Address
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApproval is a free log retrieval operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_CrossPunks *CrossPunksFilterer) FilterApproval(opts *bind.FilterOpts, owner []common.Address, approved []common.Address, tokenId []*big.Int) (*CrossPunksApprovalIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _CrossPunks.contract.FilterLogs(opts, "Approval", ownerRule, approvedRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksApprovalIterator{contract: _CrossPunks.contract, event: "Approval", logs: logs, sub: sub}, nil
}

// WatchApproval is a free log subscription operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_CrossPunks *CrossPunksFilterer) WatchApproval(opts *bind.WatchOpts, sink chan<- *CrossPunksApproval, owner []common.Address, approved []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var approvedRule []interface{}
	for _, approvedItem := range approved {
		approvedRule = append(approvedRule, approvedItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _CrossPunks.contract.WatchLogs(opts, "Approval", ownerRule, approvedRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksApproval)
				if err := _CrossPunks.contract.UnpackLog(event, "Approval", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApproval is a log parse operation binding the contract event 0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925.
//
// Solidity: event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId)
func (_CrossPunks *CrossPunksFilterer) ParseApproval(log types.Log) (*CrossPunksApproval, error) {
	event := new(CrossPunksApproval)
	if err := _CrossPunks.contract.UnpackLog(event, "Approval", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksApprovalForAllIterator is returned from FilterApprovalForAll and is used to iterate over the raw logs and unpacked data for ApprovalForAll events raised by the CrossPunks contract.
type CrossPunksApprovalForAllIterator struct {
	Event *CrossPunksApprovalForAll // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksApprovalForAllIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksApprovalForAll)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksApprovalForAll)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksApprovalForAllIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksApprovalForAllIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksApprovalForAll represents a ApprovalForAll event raised by the CrossPunks contract.
type CrossPunksApprovalForAll struct {
	Owner    common.Address
	Operator common.Address
	Approved bool
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterApprovalForAll is a free log retrieval operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_CrossPunks *CrossPunksFilterer) FilterApprovalForAll(opts *bind.FilterOpts, owner []common.Address, operator []common.Address) (*CrossPunksApprovalForAllIterator, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _CrossPunks.contract.FilterLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksApprovalForAllIterator{contract: _CrossPunks.contract, event: "ApprovalForAll", logs: logs, sub: sub}, nil
}

// WatchApprovalForAll is a free log subscription operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_CrossPunks *CrossPunksFilterer) WatchApprovalForAll(opts *bind.WatchOpts, sink chan<- *CrossPunksApprovalForAll, owner []common.Address, operator []common.Address) (event.Subscription, error) {

	var ownerRule []interface{}
	for _, ownerItem := range owner {
		ownerRule = append(ownerRule, ownerItem)
	}
	var operatorRule []interface{}
	for _, operatorItem := range operator {
		operatorRule = append(operatorRule, operatorItem)
	}

	logs, sub, err := _CrossPunks.contract.WatchLogs(opts, "ApprovalForAll", ownerRule, operatorRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksApprovalForAll)
				if err := _CrossPunks.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseApprovalForAll is a log parse operation binding the contract event 0x17307eab39ab6107e8899845ad3d59bd9653f200f220920489ca2b5937696c31.
//
// Solidity: event ApprovalForAll(address indexed owner, address indexed operator, bool approved)
func (_CrossPunks *CrossPunksFilterer) ParseApprovalForAll(log types.Log) (*CrossPunksApprovalForAll, error) {
	event := new(CrossPunksApprovalForAll)
	if err := _CrossPunks.contract.UnpackLog(event, "ApprovalForAll", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksNameChangeIterator is returned from FilterNameChange and is used to iterate over the raw logs and unpacked data for NameChange events raised by the CrossPunks contract.
type CrossPunksNameChangeIterator struct {
	Event *CrossPunksNameChange // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksNameChangeIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksNameChange)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksNameChange)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksNameChangeIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksNameChangeIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksNameChange represents a NameChange event raised by the CrossPunks contract.
type CrossPunksNameChange struct {
	MaskIndex *big.Int
	NewName   string
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterNameChange is a free log retrieval operation binding the contract event 0x7e632a301794d8d4a81ea7e20f37d1947158d36e66403af04ba85dd194b66f1b.
//
// Solidity: event NameChange(uint256 indexed maskIndex, string newName)
func (_CrossPunks *CrossPunksFilterer) FilterNameChange(opts *bind.FilterOpts, maskIndex []*big.Int) (*CrossPunksNameChangeIterator, error) {

	var maskIndexRule []interface{}
	for _, maskIndexItem := range maskIndex {
		maskIndexRule = append(maskIndexRule, maskIndexItem)
	}

	logs, sub, err := _CrossPunks.contract.FilterLogs(opts, "NameChange", maskIndexRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksNameChangeIterator{contract: _CrossPunks.contract, event: "NameChange", logs: logs, sub: sub}, nil
}

// WatchNameChange is a free log subscription operation binding the contract event 0x7e632a301794d8d4a81ea7e20f37d1947158d36e66403af04ba85dd194b66f1b.
//
// Solidity: event NameChange(uint256 indexed maskIndex, string newName)
func (_CrossPunks *CrossPunksFilterer) WatchNameChange(opts *bind.WatchOpts, sink chan<- *CrossPunksNameChange, maskIndex []*big.Int) (event.Subscription, error) {

	var maskIndexRule []interface{}
	for _, maskIndexItem := range maskIndex {
		maskIndexRule = append(maskIndexRule, maskIndexItem)
	}

	logs, sub, err := _CrossPunks.contract.WatchLogs(opts, "NameChange", maskIndexRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksNameChange)
				if err := _CrossPunks.contract.UnpackLog(event, "NameChange", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNameChange is a log parse operation binding the contract event 0x7e632a301794d8d4a81ea7e20f37d1947158d36e66403af04ba85dd194b66f1b.
//
// Solidity: event NameChange(uint256 indexed maskIndex, string newName)
func (_CrossPunks *CrossPunksFilterer) ParseNameChange(log types.Log) (*CrossPunksNameChange, error) {
	event := new(CrossPunksNameChange)
	if err := _CrossPunks.contract.UnpackLog(event, "NameChange", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksOwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the CrossPunks contract.
type CrossPunksOwnershipTransferredIterator struct {
	Event *CrossPunksOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksOwnershipTransferred represents a OwnershipTransferred event raised by the CrossPunks contract.
type CrossPunksOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_CrossPunks *CrossPunksFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*CrossPunksOwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _CrossPunks.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksOwnershipTransferredIterator{contract: _CrossPunks.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_CrossPunks *CrossPunksFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *CrossPunksOwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _CrossPunks.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksOwnershipTransferred)
				if err := _CrossPunks.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_CrossPunks *CrossPunksFilterer) ParseOwnershipTransferred(log types.Log) (*CrossPunksOwnershipTransferred, error) {
	event := new(CrossPunksOwnershipTransferred)
	if err := _CrossPunks.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksTransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the CrossPunks contract.
type CrossPunksTransferIterator struct {
	Event *CrossPunksTransfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksTransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksTransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksTransfer represents a Transfer event raised by the CrossPunks contract.
type CrossPunksTransfer struct {
	From    common.Address
	To      common.Address
	TokenId *big.Int
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_CrossPunks *CrossPunksFilterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address, tokenId []*big.Int) (*CrossPunksTransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _CrossPunks.contract.FilterLogs(opts, "Transfer", fromRule, toRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksTransferIterator{contract: _CrossPunks.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_CrossPunks *CrossPunksFilterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *CrossPunksTransfer, from []common.Address, to []common.Address, tokenId []*big.Int) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}
	var tokenIdRule []interface{}
	for _, tokenIdItem := range tokenId {
		tokenIdRule = append(tokenIdRule, tokenIdItem)
	}

	logs, sub, err := _CrossPunks.contract.WatchLogs(opts, "Transfer", fromRule, toRule, tokenIdRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksTransfer)
				if err := _CrossPunks.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 indexed tokenId)
func (_CrossPunks *CrossPunksFilterer) ParseTransfer(log types.Log) (*CrossPunksTransfer, error) {
	event := new(CrossPunksTransfer)
	if err := _CrossPunks.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
