// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package web3

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// CrossPunksDexMetaData contains all meta data concerning the CrossPunksDex contract.
var CrossPunksDexMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"crosspunks\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"operator\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_from\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"tokenId\",\"type\":\"uint256\"}],\"name\":\"ERC721Received\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"fromAddress\",\"type\":\"address\"}],\"name\":\"PunkBidEntered\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"fromAddress\",\"type\":\"address\"}],\"name\":\"PunkBidWithdrawn\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"fromAddress\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"toAddress\",\"type\":\"address\"}],\"name\":\"PunkBought\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"PunkNoLongerForSale\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"minValue\",\"type\":\"uint256\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"toAddress\",\"type\":\"address\"}],\"name\":\"PunkOffered\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"PunkTransfer\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"marketPaused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"punkBids\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"hasBid\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"bidder\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"value\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"punksOfferedForSale\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"isForSale\",\"type\":\"bool\"},{\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"minValue\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"onlySellTo\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\",\"constant\":true},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"_paused\",\"type\":\"bool\"}],\"name\":\"pauseMarket\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minSalePriceInBNB\",\"type\":\"uint256\"}],\"name\":\"offerPunkForSale\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minSalePriceInBNB\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"toAddress\",\"type\":\"address\"}],\"name\":\"offerPunkForSaleToAddress\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"buyPunk\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\",\"payable\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"enterBidForPunk\",\"outputs\":[],\"stateMutability\":\"payable\",\"type\":\"function\",\"payable\":true},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"minPrice\",\"type\":\"uint256\"}],\"name\":\"acceptBidForPunk\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"withdrawBidForPunk\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"punkIndex\",\"type\":\"uint256\"}],\"name\":\"punkNoLongerForSale\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_operator\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_from\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_tokenId\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_data\",\"type\":\"bytes\"}],\"name\":\"onERC721Received\",\"outputs\":[{\"internalType\":\"bytes4\",\"name\":\"\",\"type\":\"bytes4\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
}

// CrossPunksDexABI is the input ABI used to generate the binding from.
// Deprecated: Use CrossPunksDexMetaData.ABI instead.
var CrossPunksDexABI = CrossPunksDexMetaData.ABI

// CrossPunksDex is an auto generated Go binding around an Ethereum contract.
type CrossPunksDex struct {
	CrossPunksDexCaller     // Read-only binding to the contract
	CrossPunksDexTransactor // Write-only binding to the contract
	CrossPunksDexFilterer   // Log filterer for contract events
}

// CrossPunksDexCaller is an auto generated read-only Go binding around an Ethereum contract.
type CrossPunksDexCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// CrossPunksDexTransactor is an auto generated write-only Go binding around an Ethereum contract.
type CrossPunksDexTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// CrossPunksDexFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type CrossPunksDexFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// CrossPunksDexSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type CrossPunksDexSession struct {
	Contract     *CrossPunksDex    // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// CrossPunksDexCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type CrossPunksDexCallerSession struct {
	Contract *CrossPunksDexCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts        // Call options to use throughout this session
}

// CrossPunksDexTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type CrossPunksDexTransactorSession struct {
	Contract     *CrossPunksDexTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts        // Transaction auth options to use throughout this session
}

// CrossPunksDexRaw is an auto generated low-level Go binding around an Ethereum contract.
type CrossPunksDexRaw struct {
	Contract *CrossPunksDex // Generic contract binding to access the raw methods on
}

// CrossPunksDexCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type CrossPunksDexCallerRaw struct {
	Contract *CrossPunksDexCaller // Generic read-only contract binding to access the raw methods on
}

// CrossPunksDexTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type CrossPunksDexTransactorRaw struct {
	Contract *CrossPunksDexTransactor // Generic write-only contract binding to access the raw methods on
}

// NewCrossPunksDex creates a new instance of CrossPunksDex, bound to a specific deployed contract.
func NewCrossPunksDex(address common.Address, backend bind.ContractBackend) (*CrossPunksDex, error) {
	contract, err := bindCrossPunksDex(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDex{CrossPunksDexCaller: CrossPunksDexCaller{contract: contract}, CrossPunksDexTransactor: CrossPunksDexTransactor{contract: contract}, CrossPunksDexFilterer: CrossPunksDexFilterer{contract: contract}}, nil
}

// NewCrossPunksDexCaller creates a new read-only instance of CrossPunksDex, bound to a specific deployed contract.
func NewCrossPunksDexCaller(address common.Address, caller bind.ContractCaller) (*CrossPunksDexCaller, error) {
	contract, err := bindCrossPunksDex(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexCaller{contract: contract}, nil
}

// NewCrossPunksDexTransactor creates a new write-only instance of CrossPunksDex, bound to a specific deployed contract.
func NewCrossPunksDexTransactor(address common.Address, transactor bind.ContractTransactor) (*CrossPunksDexTransactor, error) {
	contract, err := bindCrossPunksDex(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexTransactor{contract: contract}, nil
}

// NewCrossPunksDexFilterer creates a new log filterer instance of CrossPunksDex, bound to a specific deployed contract.
func NewCrossPunksDexFilterer(address common.Address, filterer bind.ContractFilterer) (*CrossPunksDexFilterer, error) {
	contract, err := bindCrossPunksDex(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexFilterer{contract: contract}, nil
}

// bindCrossPunksDex binds a generic wrapper to an already deployed contract.
func bindCrossPunksDex(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(CrossPunksDexABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_CrossPunksDex *CrossPunksDexRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _CrossPunksDex.Contract.CrossPunksDexCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_CrossPunksDex *CrossPunksDexRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.CrossPunksDexTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_CrossPunksDex *CrossPunksDexRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.CrossPunksDexTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_CrossPunksDex *CrossPunksDexCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _CrossPunksDex.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_CrossPunksDex *CrossPunksDexTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_CrossPunksDex *CrossPunksDexTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.contract.Transact(opts, method, params...)
}

// MarketPaused is a free data retrieval call binding the contract method 0x3a283bd2.
//
// Solidity: function marketPaused() view returns(bool)
func (_CrossPunksDex *CrossPunksDexCaller) MarketPaused(opts *bind.CallOpts) (bool, error) {
	var out []interface{}
	err := _CrossPunksDex.contract.Call(opts, &out, "marketPaused")

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// MarketPaused is a free data retrieval call binding the contract method 0x3a283bd2.
//
// Solidity: function marketPaused() view returns(bool)
func (_CrossPunksDex *CrossPunksDexSession) MarketPaused() (bool, error) {
	return _CrossPunksDex.Contract.MarketPaused(&_CrossPunksDex.CallOpts)
}

// MarketPaused is a free data retrieval call binding the contract method 0x3a283bd2.
//
// Solidity: function marketPaused() view returns(bool)
func (_CrossPunksDex *CrossPunksDexCallerSession) MarketPaused() (bool, error) {
	return _CrossPunksDex.Contract.MarketPaused(&_CrossPunksDex.CallOpts)
}

// PunkBids is a free data retrieval call binding the contract method 0x6e743fa9.
//
// Solidity: function punkBids(uint256 ) view returns(bool hasBid, uint256 punkIndex, address bidder, uint256 value)
func (_CrossPunksDex *CrossPunksDexCaller) PunkBids(opts *bind.CallOpts, arg0 *big.Int) (struct {
	HasBid    bool
	PunkIndex *big.Int
	Bidder    common.Address
	Value     *big.Int
}, error) {
	var out []interface{}
	err := _CrossPunksDex.contract.Call(opts, &out, "punkBids", arg0)

	outstruct := new(struct {
		HasBid    bool
		PunkIndex *big.Int
		Bidder    common.Address
		Value     *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.HasBid = *abi.ConvertType(out[0], new(bool)).(*bool)
	outstruct.PunkIndex = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.Bidder = *abi.ConvertType(out[2], new(common.Address)).(*common.Address)
	outstruct.Value = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// PunkBids is a free data retrieval call binding the contract method 0x6e743fa9.
//
// Solidity: function punkBids(uint256 ) view returns(bool hasBid, uint256 punkIndex, address bidder, uint256 value)
func (_CrossPunksDex *CrossPunksDexSession) PunkBids(arg0 *big.Int) (struct {
	HasBid    bool
	PunkIndex *big.Int
	Bidder    common.Address
	Value     *big.Int
}, error) {
	return _CrossPunksDex.Contract.PunkBids(&_CrossPunksDex.CallOpts, arg0)
}

// PunkBids is a free data retrieval call binding the contract method 0x6e743fa9.
//
// Solidity: function punkBids(uint256 ) view returns(bool hasBid, uint256 punkIndex, address bidder, uint256 value)
func (_CrossPunksDex *CrossPunksDexCallerSession) PunkBids(arg0 *big.Int) (struct {
	HasBid    bool
	PunkIndex *big.Int
	Bidder    common.Address
	Value     *big.Int
}, error) {
	return _CrossPunksDex.Contract.PunkBids(&_CrossPunksDex.CallOpts, arg0)
}

// PunksOfferedForSale is a free data retrieval call binding the contract method 0x088f11f3.
//
// Solidity: function punksOfferedForSale(uint256 ) view returns(bool isForSale, uint256 punkIndex, address seller, uint256 minValue, address onlySellTo)
func (_CrossPunksDex *CrossPunksDexCaller) PunksOfferedForSale(opts *bind.CallOpts, arg0 *big.Int) (struct {
	IsForSale  bool
	PunkIndex  *big.Int
	Seller     common.Address
	MinValue   *big.Int
	OnlySellTo common.Address
}, error) {
	var out []interface{}
	err := _CrossPunksDex.contract.Call(opts, &out, "punksOfferedForSale", arg0)

	outstruct := new(struct {
		IsForSale  bool
		PunkIndex  *big.Int
		Seller     common.Address
		MinValue   *big.Int
		OnlySellTo common.Address
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.IsForSale = *abi.ConvertType(out[0], new(bool)).(*bool)
	outstruct.PunkIndex = *abi.ConvertType(out[1], new(*big.Int)).(**big.Int)
	outstruct.Seller = *abi.ConvertType(out[2], new(common.Address)).(*common.Address)
	outstruct.MinValue = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)
	outstruct.OnlySellTo = *abi.ConvertType(out[4], new(common.Address)).(*common.Address)

	return *outstruct, err

}

// PunksOfferedForSale is a free data retrieval call binding the contract method 0x088f11f3.
//
// Solidity: function punksOfferedForSale(uint256 ) view returns(bool isForSale, uint256 punkIndex, address seller, uint256 minValue, address onlySellTo)
func (_CrossPunksDex *CrossPunksDexSession) PunksOfferedForSale(arg0 *big.Int) (struct {
	IsForSale  bool
	PunkIndex  *big.Int
	Seller     common.Address
	MinValue   *big.Int
	OnlySellTo common.Address
}, error) {
	return _CrossPunksDex.Contract.PunksOfferedForSale(&_CrossPunksDex.CallOpts, arg0)
}

// PunksOfferedForSale is a free data retrieval call binding the contract method 0x088f11f3.
//
// Solidity: function punksOfferedForSale(uint256 ) view returns(bool isForSale, uint256 punkIndex, address seller, uint256 minValue, address onlySellTo)
func (_CrossPunksDex *CrossPunksDexCallerSession) PunksOfferedForSale(arg0 *big.Int) (struct {
	IsForSale  bool
	PunkIndex  *big.Int
	Seller     common.Address
	MinValue   *big.Int
	OnlySellTo common.Address
}, error) {
	return _CrossPunksDex.Contract.PunksOfferedForSale(&_CrossPunksDex.CallOpts, arg0)
}

// AcceptBidForPunk is a paid mutator transaction binding the contract method 0x23165b75.
//
// Solidity: function acceptBidForPunk(uint256 punkIndex, uint256 minPrice) returns()
func (_CrossPunksDex *CrossPunksDexTransactor) AcceptBidForPunk(opts *bind.TransactOpts, punkIndex *big.Int, minPrice *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.contract.Transact(opts, "acceptBidForPunk", punkIndex, minPrice)
}

// AcceptBidForPunk is a paid mutator transaction binding the contract method 0x23165b75.
//
// Solidity: function acceptBidForPunk(uint256 punkIndex, uint256 minPrice) returns()
func (_CrossPunksDex *CrossPunksDexSession) AcceptBidForPunk(punkIndex *big.Int, minPrice *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.AcceptBidForPunk(&_CrossPunksDex.TransactOpts, punkIndex, minPrice)
}

// AcceptBidForPunk is a paid mutator transaction binding the contract method 0x23165b75.
//
// Solidity: function acceptBidForPunk(uint256 punkIndex, uint256 minPrice) returns()
func (_CrossPunksDex *CrossPunksDexTransactorSession) AcceptBidForPunk(punkIndex *big.Int, minPrice *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.AcceptBidForPunk(&_CrossPunksDex.TransactOpts, punkIndex, minPrice)
}

// BuyPunk is a paid mutator transaction binding the contract method 0x8264fe98.
//
// Solidity: function buyPunk(uint256 punkIndex) payable returns()
func (_CrossPunksDex *CrossPunksDexTransactor) BuyPunk(opts *bind.TransactOpts, punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.contract.Transact(opts, "buyPunk", punkIndex)
}

// BuyPunk is a paid mutator transaction binding the contract method 0x8264fe98.
//
// Solidity: function buyPunk(uint256 punkIndex) payable returns()
func (_CrossPunksDex *CrossPunksDexSession) BuyPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.BuyPunk(&_CrossPunksDex.TransactOpts, punkIndex)
}

// BuyPunk is a paid mutator transaction binding the contract method 0x8264fe98.
//
// Solidity: function buyPunk(uint256 punkIndex) payable returns()
func (_CrossPunksDex *CrossPunksDexTransactorSession) BuyPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.BuyPunk(&_CrossPunksDex.TransactOpts, punkIndex)
}

// EnterBidForPunk is a paid mutator transaction binding the contract method 0x091dbfd2.
//
// Solidity: function enterBidForPunk(uint256 punkIndex) payable returns()
func (_CrossPunksDex *CrossPunksDexTransactor) EnterBidForPunk(opts *bind.TransactOpts, punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.contract.Transact(opts, "enterBidForPunk", punkIndex)
}

// EnterBidForPunk is a paid mutator transaction binding the contract method 0x091dbfd2.
//
// Solidity: function enterBidForPunk(uint256 punkIndex) payable returns()
func (_CrossPunksDex *CrossPunksDexSession) EnterBidForPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.EnterBidForPunk(&_CrossPunksDex.TransactOpts, punkIndex)
}

// EnterBidForPunk is a paid mutator transaction binding the contract method 0x091dbfd2.
//
// Solidity: function enterBidForPunk(uint256 punkIndex) payable returns()
func (_CrossPunksDex *CrossPunksDexTransactorSession) EnterBidForPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.EnterBidForPunk(&_CrossPunksDex.TransactOpts, punkIndex)
}

// OfferPunkForSale is a paid mutator transaction binding the contract method 0xc44193c3.
//
// Solidity: function offerPunkForSale(uint256 punkIndex, uint256 minSalePriceInBNB) returns()
func (_CrossPunksDex *CrossPunksDexTransactor) OfferPunkForSale(opts *bind.TransactOpts, punkIndex *big.Int, minSalePriceInBNB *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.contract.Transact(opts, "offerPunkForSale", punkIndex, minSalePriceInBNB)
}

// OfferPunkForSale is a paid mutator transaction binding the contract method 0xc44193c3.
//
// Solidity: function offerPunkForSale(uint256 punkIndex, uint256 minSalePriceInBNB) returns()
func (_CrossPunksDex *CrossPunksDexSession) OfferPunkForSale(punkIndex *big.Int, minSalePriceInBNB *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.OfferPunkForSale(&_CrossPunksDex.TransactOpts, punkIndex, minSalePriceInBNB)
}

// OfferPunkForSale is a paid mutator transaction binding the contract method 0xc44193c3.
//
// Solidity: function offerPunkForSale(uint256 punkIndex, uint256 minSalePriceInBNB) returns()
func (_CrossPunksDex *CrossPunksDexTransactorSession) OfferPunkForSale(punkIndex *big.Int, minSalePriceInBNB *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.OfferPunkForSale(&_CrossPunksDex.TransactOpts, punkIndex, minSalePriceInBNB)
}

// OfferPunkForSaleToAddress is a paid mutator transaction binding the contract method 0xbf31196f.
//
// Solidity: function offerPunkForSaleToAddress(uint256 punkIndex, uint256 minSalePriceInBNB, address toAddress) returns()
func (_CrossPunksDex *CrossPunksDexTransactor) OfferPunkForSaleToAddress(opts *bind.TransactOpts, punkIndex *big.Int, minSalePriceInBNB *big.Int, toAddress common.Address) (*types.Transaction, error) {
	return _CrossPunksDex.contract.Transact(opts, "offerPunkForSaleToAddress", punkIndex, minSalePriceInBNB, toAddress)
}

// OfferPunkForSaleToAddress is a paid mutator transaction binding the contract method 0xbf31196f.
//
// Solidity: function offerPunkForSaleToAddress(uint256 punkIndex, uint256 minSalePriceInBNB, address toAddress) returns()
func (_CrossPunksDex *CrossPunksDexSession) OfferPunkForSaleToAddress(punkIndex *big.Int, minSalePriceInBNB *big.Int, toAddress common.Address) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.OfferPunkForSaleToAddress(&_CrossPunksDex.TransactOpts, punkIndex, minSalePriceInBNB, toAddress)
}

// OfferPunkForSaleToAddress is a paid mutator transaction binding the contract method 0xbf31196f.
//
// Solidity: function offerPunkForSaleToAddress(uint256 punkIndex, uint256 minSalePriceInBNB, address toAddress) returns()
func (_CrossPunksDex *CrossPunksDexTransactorSession) OfferPunkForSaleToAddress(punkIndex *big.Int, minSalePriceInBNB *big.Int, toAddress common.Address) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.OfferPunkForSaleToAddress(&_CrossPunksDex.TransactOpts, punkIndex, minSalePriceInBNB, toAddress)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address _operator, address _from, uint256 _tokenId, bytes _data) returns(bytes4)
func (_CrossPunksDex *CrossPunksDexTransactor) OnERC721Received(opts *bind.TransactOpts, _operator common.Address, _from common.Address, _tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _CrossPunksDex.contract.Transact(opts, "onERC721Received", _operator, _from, _tokenId, _data)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address _operator, address _from, uint256 _tokenId, bytes _data) returns(bytes4)
func (_CrossPunksDex *CrossPunksDexSession) OnERC721Received(_operator common.Address, _from common.Address, _tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.OnERC721Received(&_CrossPunksDex.TransactOpts, _operator, _from, _tokenId, _data)
}

// OnERC721Received is a paid mutator transaction binding the contract method 0x150b7a02.
//
// Solidity: function onERC721Received(address _operator, address _from, uint256 _tokenId, bytes _data) returns(bytes4)
func (_CrossPunksDex *CrossPunksDexTransactorSession) OnERC721Received(_operator common.Address, _from common.Address, _tokenId *big.Int, _data []byte) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.OnERC721Received(&_CrossPunksDex.TransactOpts, _operator, _from, _tokenId, _data)
}

// PauseMarket is a paid mutator transaction binding the contract method 0x5ec390d8.
//
// Solidity: function pauseMarket(bool _paused) returns()
func (_CrossPunksDex *CrossPunksDexTransactor) PauseMarket(opts *bind.TransactOpts, _paused bool) (*types.Transaction, error) {
	return _CrossPunksDex.contract.Transact(opts, "pauseMarket", _paused)
}

// PauseMarket is a paid mutator transaction binding the contract method 0x5ec390d8.
//
// Solidity: function pauseMarket(bool _paused) returns()
func (_CrossPunksDex *CrossPunksDexSession) PauseMarket(_paused bool) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.PauseMarket(&_CrossPunksDex.TransactOpts, _paused)
}

// PauseMarket is a paid mutator transaction binding the contract method 0x5ec390d8.
//
// Solidity: function pauseMarket(bool _paused) returns()
func (_CrossPunksDex *CrossPunksDexTransactorSession) PauseMarket(_paused bool) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.PauseMarket(&_CrossPunksDex.TransactOpts, _paused)
}

// PunkNoLongerForSale is a paid mutator transaction binding the contract method 0xf6eeff1e.
//
// Solidity: function punkNoLongerForSale(uint256 punkIndex) returns()
func (_CrossPunksDex *CrossPunksDexTransactor) PunkNoLongerForSale(opts *bind.TransactOpts, punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.contract.Transact(opts, "punkNoLongerForSale", punkIndex)
}

// PunkNoLongerForSale is a paid mutator transaction binding the contract method 0xf6eeff1e.
//
// Solidity: function punkNoLongerForSale(uint256 punkIndex) returns()
func (_CrossPunksDex *CrossPunksDexSession) PunkNoLongerForSale(punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.PunkNoLongerForSale(&_CrossPunksDex.TransactOpts, punkIndex)
}

// PunkNoLongerForSale is a paid mutator transaction binding the contract method 0xf6eeff1e.
//
// Solidity: function punkNoLongerForSale(uint256 punkIndex) returns()
func (_CrossPunksDex *CrossPunksDexTransactorSession) PunkNoLongerForSale(punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.PunkNoLongerForSale(&_CrossPunksDex.TransactOpts, punkIndex)
}

// WithdrawBidForPunk is a paid mutator transaction binding the contract method 0x979bc638.
//
// Solidity: function withdrawBidForPunk(uint256 punkIndex) returns()
func (_CrossPunksDex *CrossPunksDexTransactor) WithdrawBidForPunk(opts *bind.TransactOpts, punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.contract.Transact(opts, "withdrawBidForPunk", punkIndex)
}

// WithdrawBidForPunk is a paid mutator transaction binding the contract method 0x979bc638.
//
// Solidity: function withdrawBidForPunk(uint256 punkIndex) returns()
func (_CrossPunksDex *CrossPunksDexSession) WithdrawBidForPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.WithdrawBidForPunk(&_CrossPunksDex.TransactOpts, punkIndex)
}

// WithdrawBidForPunk is a paid mutator transaction binding the contract method 0x979bc638.
//
// Solidity: function withdrawBidForPunk(uint256 punkIndex) returns()
func (_CrossPunksDex *CrossPunksDexTransactorSession) WithdrawBidForPunk(punkIndex *big.Int) (*types.Transaction, error) {
	return _CrossPunksDex.Contract.WithdrawBidForPunk(&_CrossPunksDex.TransactOpts, punkIndex)
}

// CrossPunksDexERC721ReceivedIterator is returned from FilterERC721Received and is used to iterate over the raw logs and unpacked data for ERC721Received events raised by the CrossPunksDex contract.
type CrossPunksDexERC721ReceivedIterator struct {
	Event *CrossPunksDexERC721Received // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksDexERC721ReceivedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksDexERC721Received)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksDexERC721Received)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksDexERC721ReceivedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksDexERC721ReceivedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksDexERC721Received represents a ERC721Received event raised by the CrossPunksDex contract.
type CrossPunksDexERC721Received struct {
	Operator common.Address
	From     common.Address
	TokenId  *big.Int
	Raw      types.Log // Blockchain specific contextual infos
}

// FilterERC721Received is a free log retrieval operation binding the contract event 0x941a9d9a9af7f97737c018d13918859e12f44e16c4b4c6ac922dacf021cfbf14.
//
// Solidity: event ERC721Received(address operator, address _from, uint256 tokenId)
func (_CrossPunksDex *CrossPunksDexFilterer) FilterERC721Received(opts *bind.FilterOpts) (*CrossPunksDexERC721ReceivedIterator, error) {

	logs, sub, err := _CrossPunksDex.contract.FilterLogs(opts, "ERC721Received")
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexERC721ReceivedIterator{contract: _CrossPunksDex.contract, event: "ERC721Received", logs: logs, sub: sub}, nil
}

// WatchERC721Received is a free log subscription operation binding the contract event 0x941a9d9a9af7f97737c018d13918859e12f44e16c4b4c6ac922dacf021cfbf14.
//
// Solidity: event ERC721Received(address operator, address _from, uint256 tokenId)
func (_CrossPunksDex *CrossPunksDexFilterer) WatchERC721Received(opts *bind.WatchOpts, sink chan<- *CrossPunksDexERC721Received) (event.Subscription, error) {

	logs, sub, err := _CrossPunksDex.contract.WatchLogs(opts, "ERC721Received")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksDexERC721Received)
				if err := _CrossPunksDex.contract.UnpackLog(event, "ERC721Received", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseERC721Received is a log parse operation binding the contract event 0x941a9d9a9af7f97737c018d13918859e12f44e16c4b4c6ac922dacf021cfbf14.
//
// Solidity: event ERC721Received(address operator, address _from, uint256 tokenId)
func (_CrossPunksDex *CrossPunksDexFilterer) ParseERC721Received(log types.Log) (*CrossPunksDexERC721Received, error) {
	event := new(CrossPunksDexERC721Received)
	if err := _CrossPunksDex.contract.UnpackLog(event, "ERC721Received", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksDexPunkBidEnteredIterator is returned from FilterPunkBidEntered and is used to iterate over the raw logs and unpacked data for PunkBidEntered events raised by the CrossPunksDex contract.
type CrossPunksDexPunkBidEnteredIterator struct {
	Event *CrossPunksDexPunkBidEntered // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksDexPunkBidEnteredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksDexPunkBidEntered)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksDexPunkBidEntered)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksDexPunkBidEnteredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksDexPunkBidEnteredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksDexPunkBidEntered represents a PunkBidEntered event raised by the CrossPunksDex contract.
type CrossPunksDexPunkBidEntered struct {
	PunkIndex   *big.Int
	Value       *big.Int
	FromAddress common.Address
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterPunkBidEntered is a free log retrieval operation binding the contract event 0x5b859394fabae0c1ba88baffe67e751ab5248d2e879028b8c8d6897b0519f56a.
//
// Solidity: event PunkBidEntered(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) FilterPunkBidEntered(opts *bind.FilterOpts, punkIndex []*big.Int, fromAddress []common.Address) (*CrossPunksDexPunkBidEnteredIterator, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}

	logs, sub, err := _CrossPunksDex.contract.FilterLogs(opts, "PunkBidEntered", punkIndexRule, fromAddressRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexPunkBidEnteredIterator{contract: _CrossPunksDex.contract, event: "PunkBidEntered", logs: logs, sub: sub}, nil
}

// WatchPunkBidEntered is a free log subscription operation binding the contract event 0x5b859394fabae0c1ba88baffe67e751ab5248d2e879028b8c8d6897b0519f56a.
//
// Solidity: event PunkBidEntered(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) WatchPunkBidEntered(opts *bind.WatchOpts, sink chan<- *CrossPunksDexPunkBidEntered, punkIndex []*big.Int, fromAddress []common.Address) (event.Subscription, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}

	logs, sub, err := _CrossPunksDex.contract.WatchLogs(opts, "PunkBidEntered", punkIndexRule, fromAddressRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksDexPunkBidEntered)
				if err := _CrossPunksDex.contract.UnpackLog(event, "PunkBidEntered", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkBidEntered is a log parse operation binding the contract event 0x5b859394fabae0c1ba88baffe67e751ab5248d2e879028b8c8d6897b0519f56a.
//
// Solidity: event PunkBidEntered(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) ParsePunkBidEntered(log types.Log) (*CrossPunksDexPunkBidEntered, error) {
	event := new(CrossPunksDexPunkBidEntered)
	if err := _CrossPunksDex.contract.UnpackLog(event, "PunkBidEntered", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksDexPunkBidWithdrawnIterator is returned from FilterPunkBidWithdrawn and is used to iterate over the raw logs and unpacked data for PunkBidWithdrawn events raised by the CrossPunksDex contract.
type CrossPunksDexPunkBidWithdrawnIterator struct {
	Event *CrossPunksDexPunkBidWithdrawn // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksDexPunkBidWithdrawnIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksDexPunkBidWithdrawn)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksDexPunkBidWithdrawn)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksDexPunkBidWithdrawnIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksDexPunkBidWithdrawnIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksDexPunkBidWithdrawn represents a PunkBidWithdrawn event raised by the CrossPunksDex contract.
type CrossPunksDexPunkBidWithdrawn struct {
	PunkIndex   *big.Int
	Value       *big.Int
	FromAddress common.Address
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterPunkBidWithdrawn is a free log retrieval operation binding the contract event 0x6f30e1ee4d81dcc7a8a478577f65d2ed2edb120565960ac45fe7c50551c87932.
//
// Solidity: event PunkBidWithdrawn(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) FilterPunkBidWithdrawn(opts *bind.FilterOpts, punkIndex []*big.Int, fromAddress []common.Address) (*CrossPunksDexPunkBidWithdrawnIterator, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}

	logs, sub, err := _CrossPunksDex.contract.FilterLogs(opts, "PunkBidWithdrawn", punkIndexRule, fromAddressRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexPunkBidWithdrawnIterator{contract: _CrossPunksDex.contract, event: "PunkBidWithdrawn", logs: logs, sub: sub}, nil
}

// WatchPunkBidWithdrawn is a free log subscription operation binding the contract event 0x6f30e1ee4d81dcc7a8a478577f65d2ed2edb120565960ac45fe7c50551c87932.
//
// Solidity: event PunkBidWithdrawn(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) WatchPunkBidWithdrawn(opts *bind.WatchOpts, sink chan<- *CrossPunksDexPunkBidWithdrawn, punkIndex []*big.Int, fromAddress []common.Address) (event.Subscription, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}

	logs, sub, err := _CrossPunksDex.contract.WatchLogs(opts, "PunkBidWithdrawn", punkIndexRule, fromAddressRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksDexPunkBidWithdrawn)
				if err := _CrossPunksDex.contract.UnpackLog(event, "PunkBidWithdrawn", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkBidWithdrawn is a log parse operation binding the contract event 0x6f30e1ee4d81dcc7a8a478577f65d2ed2edb120565960ac45fe7c50551c87932.
//
// Solidity: event PunkBidWithdrawn(uint256 indexed punkIndex, uint256 value, address indexed fromAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) ParsePunkBidWithdrawn(log types.Log) (*CrossPunksDexPunkBidWithdrawn, error) {
	event := new(CrossPunksDexPunkBidWithdrawn)
	if err := _CrossPunksDex.contract.UnpackLog(event, "PunkBidWithdrawn", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksDexPunkBoughtIterator is returned from FilterPunkBought and is used to iterate over the raw logs and unpacked data for PunkBought events raised by the CrossPunksDex contract.
type CrossPunksDexPunkBoughtIterator struct {
	Event *CrossPunksDexPunkBought // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksDexPunkBoughtIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksDexPunkBought)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksDexPunkBought)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksDexPunkBoughtIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksDexPunkBoughtIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksDexPunkBought represents a PunkBought event raised by the CrossPunksDex contract.
type CrossPunksDexPunkBought struct {
	PunkIndex   *big.Int
	Value       *big.Int
	FromAddress common.Address
	ToAddress   common.Address
	Raw         types.Log // Blockchain specific contextual infos
}

// FilterPunkBought is a free log retrieval operation binding the contract event 0x58e5d5a525e3b40bc15abaa38b5882678db1ee68befd2f60bafe3a7fd06db9e3.
//
// Solidity: event PunkBought(uint256 indexed punkIndex, uint256 value, address indexed fromAddress, address indexed toAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) FilterPunkBought(opts *bind.FilterOpts, punkIndex []*big.Int, fromAddress []common.Address, toAddress []common.Address) (*CrossPunksDexPunkBoughtIterator, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}
	var toAddressRule []interface{}
	for _, toAddressItem := range toAddress {
		toAddressRule = append(toAddressRule, toAddressItem)
	}

	logs, sub, err := _CrossPunksDex.contract.FilterLogs(opts, "PunkBought", punkIndexRule, fromAddressRule, toAddressRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexPunkBoughtIterator{contract: _CrossPunksDex.contract, event: "PunkBought", logs: logs, sub: sub}, nil
}

// WatchPunkBought is a free log subscription operation binding the contract event 0x58e5d5a525e3b40bc15abaa38b5882678db1ee68befd2f60bafe3a7fd06db9e3.
//
// Solidity: event PunkBought(uint256 indexed punkIndex, uint256 value, address indexed fromAddress, address indexed toAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) WatchPunkBought(opts *bind.WatchOpts, sink chan<- *CrossPunksDexPunkBought, punkIndex []*big.Int, fromAddress []common.Address, toAddress []common.Address) (event.Subscription, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var fromAddressRule []interface{}
	for _, fromAddressItem := range fromAddress {
		fromAddressRule = append(fromAddressRule, fromAddressItem)
	}
	var toAddressRule []interface{}
	for _, toAddressItem := range toAddress {
		toAddressRule = append(toAddressRule, toAddressItem)
	}

	logs, sub, err := _CrossPunksDex.contract.WatchLogs(opts, "PunkBought", punkIndexRule, fromAddressRule, toAddressRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksDexPunkBought)
				if err := _CrossPunksDex.contract.UnpackLog(event, "PunkBought", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkBought is a log parse operation binding the contract event 0x58e5d5a525e3b40bc15abaa38b5882678db1ee68befd2f60bafe3a7fd06db9e3.
//
// Solidity: event PunkBought(uint256 indexed punkIndex, uint256 value, address indexed fromAddress, address indexed toAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) ParsePunkBought(log types.Log) (*CrossPunksDexPunkBought, error) {
	event := new(CrossPunksDexPunkBought)
	if err := _CrossPunksDex.contract.UnpackLog(event, "PunkBought", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksDexPunkNoLongerForSaleIterator is returned from FilterPunkNoLongerForSale and is used to iterate over the raw logs and unpacked data for PunkNoLongerForSale events raised by the CrossPunksDex contract.
type CrossPunksDexPunkNoLongerForSaleIterator struct {
	Event *CrossPunksDexPunkNoLongerForSale // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksDexPunkNoLongerForSaleIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksDexPunkNoLongerForSale)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksDexPunkNoLongerForSale)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksDexPunkNoLongerForSaleIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksDexPunkNoLongerForSaleIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksDexPunkNoLongerForSale represents a PunkNoLongerForSale event raised by the CrossPunksDex contract.
type CrossPunksDexPunkNoLongerForSale struct {
	PunkIndex *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterPunkNoLongerForSale is a free log retrieval operation binding the contract event 0xb0e0a660b4e50f26f0b7ce75c24655fc76cc66e3334a54ff410277229fa10bd4.
//
// Solidity: event PunkNoLongerForSale(uint256 indexed punkIndex)
func (_CrossPunksDex *CrossPunksDexFilterer) FilterPunkNoLongerForSale(opts *bind.FilterOpts, punkIndex []*big.Int) (*CrossPunksDexPunkNoLongerForSaleIterator, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	logs, sub, err := _CrossPunksDex.contract.FilterLogs(opts, "PunkNoLongerForSale", punkIndexRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexPunkNoLongerForSaleIterator{contract: _CrossPunksDex.contract, event: "PunkNoLongerForSale", logs: logs, sub: sub}, nil
}

// WatchPunkNoLongerForSale is a free log subscription operation binding the contract event 0xb0e0a660b4e50f26f0b7ce75c24655fc76cc66e3334a54ff410277229fa10bd4.
//
// Solidity: event PunkNoLongerForSale(uint256 indexed punkIndex)
func (_CrossPunksDex *CrossPunksDexFilterer) WatchPunkNoLongerForSale(opts *bind.WatchOpts, sink chan<- *CrossPunksDexPunkNoLongerForSale, punkIndex []*big.Int) (event.Subscription, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	logs, sub, err := _CrossPunksDex.contract.WatchLogs(opts, "PunkNoLongerForSale", punkIndexRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksDexPunkNoLongerForSale)
				if err := _CrossPunksDex.contract.UnpackLog(event, "PunkNoLongerForSale", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkNoLongerForSale is a log parse operation binding the contract event 0xb0e0a660b4e50f26f0b7ce75c24655fc76cc66e3334a54ff410277229fa10bd4.
//
// Solidity: event PunkNoLongerForSale(uint256 indexed punkIndex)
func (_CrossPunksDex *CrossPunksDexFilterer) ParsePunkNoLongerForSale(log types.Log) (*CrossPunksDexPunkNoLongerForSale, error) {
	event := new(CrossPunksDexPunkNoLongerForSale)
	if err := _CrossPunksDex.contract.UnpackLog(event, "PunkNoLongerForSale", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksDexPunkOfferedIterator is returned from FilterPunkOffered and is used to iterate over the raw logs and unpacked data for PunkOffered events raised by the CrossPunksDex contract.
type CrossPunksDexPunkOfferedIterator struct {
	Event *CrossPunksDexPunkOffered // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksDexPunkOfferedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksDexPunkOffered)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksDexPunkOffered)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksDexPunkOfferedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksDexPunkOfferedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksDexPunkOffered represents a PunkOffered event raised by the CrossPunksDex contract.
type CrossPunksDexPunkOffered struct {
	PunkIndex *big.Int
	MinValue  *big.Int
	ToAddress common.Address
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterPunkOffered is a free log retrieval operation binding the contract event 0x3c7b682d5da98001a9b8cbda6c647d2c63d698a4184fd1d55e2ce7b66f5d21eb.
//
// Solidity: event PunkOffered(uint256 indexed punkIndex, uint256 minValue, address indexed toAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) FilterPunkOffered(opts *bind.FilterOpts, punkIndex []*big.Int, toAddress []common.Address) (*CrossPunksDexPunkOfferedIterator, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var toAddressRule []interface{}
	for _, toAddressItem := range toAddress {
		toAddressRule = append(toAddressRule, toAddressItem)
	}

	logs, sub, err := _CrossPunksDex.contract.FilterLogs(opts, "PunkOffered", punkIndexRule, toAddressRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexPunkOfferedIterator{contract: _CrossPunksDex.contract, event: "PunkOffered", logs: logs, sub: sub}, nil
}

// WatchPunkOffered is a free log subscription operation binding the contract event 0x3c7b682d5da98001a9b8cbda6c647d2c63d698a4184fd1d55e2ce7b66f5d21eb.
//
// Solidity: event PunkOffered(uint256 indexed punkIndex, uint256 minValue, address indexed toAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) WatchPunkOffered(opts *bind.WatchOpts, sink chan<- *CrossPunksDexPunkOffered, punkIndex []*big.Int, toAddress []common.Address) (event.Subscription, error) {

	var punkIndexRule []interface{}
	for _, punkIndexItem := range punkIndex {
		punkIndexRule = append(punkIndexRule, punkIndexItem)
	}

	var toAddressRule []interface{}
	for _, toAddressItem := range toAddress {
		toAddressRule = append(toAddressRule, toAddressItem)
	}

	logs, sub, err := _CrossPunksDex.contract.WatchLogs(opts, "PunkOffered", punkIndexRule, toAddressRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksDexPunkOffered)
				if err := _CrossPunksDex.contract.UnpackLog(event, "PunkOffered", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkOffered is a log parse operation binding the contract event 0x3c7b682d5da98001a9b8cbda6c647d2c63d698a4184fd1d55e2ce7b66f5d21eb.
//
// Solidity: event PunkOffered(uint256 indexed punkIndex, uint256 minValue, address indexed toAddress)
func (_CrossPunksDex *CrossPunksDexFilterer) ParsePunkOffered(log types.Log) (*CrossPunksDexPunkOffered, error) {
	event := new(CrossPunksDexPunkOffered)
	if err := _CrossPunksDex.contract.UnpackLog(event, "PunkOffered", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksDexPunkTransferIterator is returned from FilterPunkTransfer and is used to iterate over the raw logs and unpacked data for PunkTransfer events raised by the CrossPunksDex contract.
type CrossPunksDexPunkTransferIterator struct {
	Event *CrossPunksDexPunkTransfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksDexPunkTransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksDexPunkTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksDexPunkTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksDexPunkTransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksDexPunkTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksDexPunkTransfer represents a PunkTransfer event raised by the CrossPunksDex contract.
type CrossPunksDexPunkTransfer struct {
	From      common.Address
	To        common.Address
	PunkIndex *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterPunkTransfer is a free log retrieval operation binding the contract event 0x05af636b70da6819000c49f85b21fa82081c632069bb626f30932034099107d8.
//
// Solidity: event PunkTransfer(address indexed from, address indexed to, uint256 punkIndex)
func (_CrossPunksDex *CrossPunksDexFilterer) FilterPunkTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*CrossPunksDexPunkTransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _CrossPunksDex.contract.FilterLogs(opts, "PunkTransfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexPunkTransferIterator{contract: _CrossPunksDex.contract, event: "PunkTransfer", logs: logs, sub: sub}, nil
}

// WatchPunkTransfer is a free log subscription operation binding the contract event 0x05af636b70da6819000c49f85b21fa82081c632069bb626f30932034099107d8.
//
// Solidity: event PunkTransfer(address indexed from, address indexed to, uint256 punkIndex)
func (_CrossPunksDex *CrossPunksDexFilterer) WatchPunkTransfer(opts *bind.WatchOpts, sink chan<- *CrossPunksDexPunkTransfer, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _CrossPunksDex.contract.WatchLogs(opts, "PunkTransfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksDexPunkTransfer)
				if err := _CrossPunksDex.contract.UnpackLog(event, "PunkTransfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParsePunkTransfer is a log parse operation binding the contract event 0x05af636b70da6819000c49f85b21fa82081c632069bb626f30932034099107d8.
//
// Solidity: event PunkTransfer(address indexed from, address indexed to, uint256 punkIndex)
func (_CrossPunksDex *CrossPunksDexFilterer) ParsePunkTransfer(log types.Log) (*CrossPunksDexPunkTransfer, error) {
	event := new(CrossPunksDexPunkTransfer)
	if err := _CrossPunksDex.contract.UnpackLog(event, "PunkTransfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// CrossPunksDexTransferIterator is returned from FilterTransfer and is used to iterate over the raw logs and unpacked data for Transfer events raised by the CrossPunksDex contract.
type CrossPunksDexTransferIterator struct {
	Event *CrossPunksDexTransfer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *CrossPunksDexTransferIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(CrossPunksDexTransfer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(CrossPunksDexTransfer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *CrossPunksDexTransferIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *CrossPunksDexTransferIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// CrossPunksDexTransfer represents a Transfer event raised by the CrossPunksDex contract.
type CrossPunksDexTransfer struct {
	From  common.Address
	To    common.Address
	Value *big.Int
	Raw   types.Log // Blockchain specific contextual infos
}

// FilterTransfer is a free log retrieval operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_CrossPunksDex *CrossPunksDexFilterer) FilterTransfer(opts *bind.FilterOpts, from []common.Address, to []common.Address) (*CrossPunksDexTransferIterator, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _CrossPunksDex.contract.FilterLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return &CrossPunksDexTransferIterator{contract: _CrossPunksDex.contract, event: "Transfer", logs: logs, sub: sub}, nil
}

// WatchTransfer is a free log subscription operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_CrossPunksDex *CrossPunksDexFilterer) WatchTransfer(opts *bind.WatchOpts, sink chan<- *CrossPunksDexTransfer, from []common.Address, to []common.Address) (event.Subscription, error) {

	var fromRule []interface{}
	for _, fromItem := range from {
		fromRule = append(fromRule, fromItem)
	}
	var toRule []interface{}
	for _, toItem := range to {
		toRule = append(toRule, toItem)
	}

	logs, sub, err := _CrossPunksDex.contract.WatchLogs(opts, "Transfer", fromRule, toRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(CrossPunksDexTransfer)
				if err := _CrossPunksDex.contract.UnpackLog(event, "Transfer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseTransfer is a log parse operation binding the contract event 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef.
//
// Solidity: event Transfer(address indexed from, address indexed to, uint256 value)
func (_CrossPunksDex *CrossPunksDexFilterer) ParseTransfer(log types.Log) (*CrossPunksDexTransfer, error) {
	event := new(CrossPunksDexTransfer)
	if err := _CrossPunksDex.contract.UnpackLog(event, "Transfer", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
