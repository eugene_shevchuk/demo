package server

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"crosspunks/types"
)

func SetupRouter(logger *zap.Logger, app CrossPunksApp) http.Handler {
	r := &router{
		app:    app,
		logger: logger,
	}
	return r.Handler()
}

type CrossPunksApp interface {
	GetPunkByIdx(ctx context.Context, idx int64) (*types.Punk, error)
	GetPunks(ctx context.Context, filter *types.PunksFilter) ([]*types.Punk, error)
	Count(ctx context.Context, filter *types.PunksFilter) (int, error)
	Sync(ctx context.Context)
}

type router struct {
	app    CrossPunksApp
	logger *zap.Logger
}

func (ro *router) Handler() http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/punks", ro.punksHandler).Methods("GET")
	r.HandleFunc("/punks/{idx}", ro.punksByIdxHandler).Methods("GET")
	r.HandleFunc("/punks/sync", ro.punksSyncHandler).Methods("POST")
	return r
}

func (ro *router) punksHandler(w http.ResponseWriter, r *http.Request) {
	var filter types.PunksFilter
	decoder := schema.NewDecoder()
	err := decoder.Decode(&filter, r.URL.Query())
	if err != nil {
		ro.httpJSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if filter.Limit == 0 {
		filter.Limit = 10
	}

	punks, err := ro.app.GetPunks(r.Context(), &filter)
	if err != nil {
		ro.handlerError(w, err)
		return
	}

	count, err := ro.app.Count(r.Context(), &filter)
	if err != nil {
		ro.handlerError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(&types.PunksResponse{
		Punks: punks,
		Total: count,
	}); err != nil {
		ro.logger.Warn("error writing json response", zap.Error(err))
	}
}

func (ro *router) punksByIdxHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	idx, err := strconv.ParseInt(vars["idx"], 10, 64)
	if err != nil {
		ro.httpJSONError(w, err.Error(), http.StatusBadRequest)
		return
	}
	punk, err := ro.app.GetPunkByIdx(r.Context(), idx)
	if err != nil {
		ro.handlerError(w, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err = json.NewEncoder(w).Encode(punk); err != nil {
		ro.logger.Warn("error writing json response", zap.Error(err))
	}
}

func (ro *router) punksSyncHandler(w http.ResponseWriter, r *http.Request) {
	ro.app.Sync(context.Background())
	w.WriteHeader(http.StatusNoContent)
}

func (ro *router) handlerError(w http.ResponseWriter, err error) {
	if errors.Cause(err) == types.ErrNoObject {
		ro.httpJSONError(w, err.Error(), http.StatusNotFound)
		return
	}

	ro.httpJSONError(w, err.Error(), http.StatusInternalServerError)
}

func (ro *router) httpJSONError(w http.ResponseWriter, error string, code int) {
	w.WriteHeader(code)
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(map[string]string{
		"error": error,
	})
	if err != nil {
		ro.logger.Warn("error writing json response", zap.Error(err))
	}
}
