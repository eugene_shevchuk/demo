package docs

import "crosspunks/types"

// Schemes: https
// BasePath: /
// Host: localhost:8080
// Version: 1.0.0
//
// Consumes:
//   - application/json
//
// Produces:
//   - application/json
//
// swagger:meta

// swagger:route GET /punks/{idx} punkByIdx
// responses:
//   200: punkByIdx

// swagger:response punkByIdx
// nolint
type punksByIdxWrapper struct {
	// in:body
	Body types.Punk
}

// swagger:route GET /punks punks
// responses:
//   200: punks

// swagger:response punks
// nolint
type punksWrapper struct {
	// in:body
	Body types.PunksResponse
}
