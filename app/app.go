package app

import (
	"context"
	"database/sql"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"crosspunks/config"
	"crosspunks/types"
	"crosspunks/web3"
)

type PunksStore interface {
	Update(context.Context, *types.Punk) (*types.Punk, error)
	GetByIdx(context.Context, int64) (*types.Punk, error)
	GetPunks(context.Context, *types.PunksFilter) ([]*types.Punk, error)
	Count(context.Context, *types.PunksFilter) (int, error)
}

type BlockStore interface {
	GetLastBlock(context.Context) (uint64, error)
	Update(context.Context, uint64) (uint64, error)
}

type App struct {
	logger *zap.Logger
	cfg    config.Config

	ethClient  *ethclient.Client
	nft        *web3.CrossPunks
	dex        *web3.CrossPunksDex
	punksStore PunksStore
	blockStore BlockStore

	isSync bool
}

func NewApp(logger *zap.Logger, cfg config.Config, punksStore PunksStore, blockStore BlockStore) (*App, error) {
	ethClient, err := ethclient.Dial(cfg.Eth.RpcURL)
	if err != nil {
		return nil, err
	}

	nft, err := web3.NewCrossPunks(common.HexToAddress(cfg.Eth.NftAddress), ethClient)
	if err != nil {
		return nil, err
	}

	dex, err := web3.NewCrossPunksDex(common.HexToAddress(cfg.Eth.DexAddress), ethClient)
	if err != nil {
		return nil, err
	}

	return &App{
		logger:     logger,
		cfg:        cfg,
		punksStore: punksStore,
		blockStore: blockStore,
		ethClient:  ethClient,
		nft:        nft,
		dex:        dex,
	}, nil
}

func (app *App) GetPunkByIdx(ctx context.Context, idx int64) (*types.Punk, error) {
	punk, err := app.punksStore.GetByIdx(ctx, idx)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			err = types.ErrNoObject
		}
		return nil, err
	}
	return punk, err
}

func (app *App) GetPunks(ctx context.Context, filter *types.PunksFilter) ([]*types.Punk, error) {
	punks, err := app.punksStore.GetPunks(ctx, filter)
	if err != nil {
		return nil, err
	}
	return punks, nil
}

func (app *App) Count(ctx context.Context, filter *types.PunksFilter) (int, error) {
	return app.punksStore.Count(ctx, filter)
}

func (app *App) UpdatePunk(ctx context.Context, punk *types.Punk) error {
	oldPunk, err := app.punksStore.GetByIdx(ctx, punk.Idx)
	if err != nil {
		return err
	}
	if oldPunk.Block >= punk.Block {
		return nil
	}
	punk, err = app.punksStore.Update(ctx, punk)
	if err != nil {
		return err
	}
	app.logger.Debug("successful handle punk", zap.Int64("idx", punk.Idx))
	return nil
}
