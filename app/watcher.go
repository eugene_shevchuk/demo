package app

import (
	"context"
	"crosspunks/web3"
	"fmt"
	"github.com/ethereum/go-ethereum/ethclient"
	"math/big"
	"time"

	"github.com/avast/retry-go"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"go.uber.org/zap"

	"crosspunks/types"
)

const nonexistentTokenErr = "execution reverted: ERC721: owner query for nonexistent token"

func (app *App) WithRetry(fn func() error, warn string) error {
	return retry.Do(fn,
		retry.Attempts(app.cfg.RetryAttempts),
		retry.Delay(app.cfg.RetryDelay*time.Millisecond),
		retry.OnRetry(func(n uint, err error) {
			app.logger.Warn(warn, zap.Uint("attempt", n), zap.Error(err))
		}))
}

func (app *App) GetPunk(idx int64) (*types.Punk, error) {
	var (
		blockNumber uint64
		owner       common.Address
		offer       struct {
			IsForSale  bool
			PunkIndex  *big.Int
			Seller     common.Address
			MinValue   *big.Int
			OnlySellTo common.Address
		}
		bid struct {
			HasBid    bool
			PunkIndex *big.Int
			Bidder    common.Address
			Value     *big.Int
		}
		innerErr error
	)
	err := app.WithRetry(func() error {
		blockNumber, innerErr = app.ethClient.BlockNumber(context.Background())
		if innerErr != nil {
			return innerErr
		}
		return nil
	}, fmt.Sprintf("failted get block number attempt by idx: %d", idx))
	if err != nil {
		app.logger.Error("failed to get block number", zap.Error(err))
		return nil, err
	}

	block := int64(blockNumber)
	opts := &bind.CallOpts{BlockNumber: big.NewInt(block)}
	punk := &types.Punk{
		Idx:   idx,
		Block: int64(blockNumber),
	}

	err = app.WithRetry(func() error {
		owner, innerErr = app.nft.OwnerOf(opts, big.NewInt(idx))
		if innerErr != nil {
			if innerErr.Error() == nonexistentTokenErr {
				return nil
			}
			return innerErr
		}
		return nil
	}, fmt.Sprintf("failted get owner attempt by idx: %d", idx))
	if owner.String() != types.NilAddress.String() {
		ownerStr := owner.String()
		punk.Owner = &ownerStr
	}
	if err != nil {
		app.logger.Warn("failed to get owner", zap.Error(err), zap.Int64("idx", idx))
	}
	err = app.WithRetry(func() error {
		offer, innerErr = app.dex.PunksOfferedForSale(opts, big.NewInt(idx))
		if innerErr != nil {
			return innerErr
		}
		return nil
	}, fmt.Sprintf("failted get offer attempt by idx: %d", idx))
	if err == nil && offer.IsForSale {
		punk.Offer = &types.Offer{
			Seller:   offer.Seller.String(),
			MinValue: offer.MinValue.String(),
		}
		if offer.OnlySellTo.String() != types.NilAddress.String() {
			toAddress := offer.OnlySellTo.String()
			punk.Offer.ToAddress = &toAddress
		}
	}
	if err != nil {
		app.logger.Warn("failed to get offer", zap.Error(err), zap.Int64("idx", idx))
	}

	err = app.WithRetry(func() error {
		bid, innerErr = app.dex.PunkBids(opts, big.NewInt(idx))
		if innerErr != nil {
			return innerErr
		}
		return nil
	}, fmt.Sprintf("failted get bid attempt by idx: %d", idx))
	if err == nil && bid.HasBid {
		punk.Bid = &types.Bid{
			Bidder: bid.Bidder.String(),
			Value:  bid.Value.String(),
		}
	}
	if err != nil {
		app.logger.Warn("failed to get bid", zap.Error(err), zap.Int64("idx", idx))
	}

	return punk, nil
}

func (app *App) Start(ctx context.Context) {
	if app.cfg.IsNeedSync {
		app.Sync(ctx)
	}
	go func(ctx context.Context) {
		for {
			select {
			case <-time.After(3 * time.Second):
				var err error
				app.ethClient, err = ethclient.Dial(app.cfg.Eth.RpcURL)
				if err != nil {
					app.logger.Error("failed to update client", zap.Error(err))
					continue
				}

				app.nft, err = web3.NewCrossPunks(common.HexToAddress(app.cfg.Eth.NftAddress), app.ethClient)
				if err != nil {
					app.logger.Error("failed to generate nft abi", zap.Error(err))
					continue
				}

				app.dex, err = web3.NewCrossPunksDex(common.HexToAddress(app.cfg.Eth.DexAddress), app.ethClient)
				if err != nil {
					app.logger.Error("failed to generate dex abi", zap.Error(err))
					continue
				}
				lastBlockNumber, err := app.blockStore.GetLastBlock(ctx)
				if err != nil {
					app.logger.Error("failed to get last block number", zap.Error(err))
					continue
				}
				blockNumber, err := app.ethClient.BlockNumber(ctx)
				if err != nil {
					app.logger.Error("failed to get block number", zap.Error(err))
					continue
				}
				if blockNumber <= lastBlockNumber {
					continue
				}

				if blockNumber-lastBlockNumber > 1000 {
					blockNumber = lastBlockNumber + 1000
				}

				start := lastBlockNumber
				end := blockNumber
				if end-start > 1000 {
					end = start + 1000
				}
				for {
					app.logger.Info("getting new events...", zap.Uint64("start", start), zap.Uint64("end", end))
					opts := &bind.FilterOpts{
						Start:   start,
						End:     &end,
						Context: ctx,
					}
					transfers, err := app.nft.FilterTransfer(opts, nil, nil, nil)
					if err != nil {
						app.logger.Error("failed to filter transfers", zap.Error(err))
						continue
					}
					for transfers.Next() {
						idx := transfers.Event.TokenId.Int64()
						err := app.SyncPunk(ctx, idx)
						if err != nil {
							app.logger.Error("failed to sync punk", zap.Error(err), zap.Int64("idx", idx))
							continue
						}
					}
					_ = transfers.Close()

					bids, err := app.dex.FilterPunkBidEntered(opts, nil, nil)
					if err != nil {
						app.logger.Error("failed to filter bids", zap.Error(err))
						continue
					}

					for bids.Next() {
						idx := bids.Event.PunkIndex.Int64()
						err := app.SyncPunk(ctx, idx)
						if err != nil {
							app.logger.Error("failed to sync punk", zap.Error(err), zap.Int64("idx", idx))
							continue
						}
					}
					_ = bids.Close()

					if end >= blockNumber {
						break
					}
					start = end
					end = start + 1000
					if end > blockNumber {
						end = blockNumber
					}
				}

				_, err = app.blockStore.Update(ctx, blockNumber)
				if err != nil {
					app.logger.Error("failed to update last block number", zap.Error(err))
					continue
				}

				app.ethClient.Close()

			case <-ctx.Done():
				return
			}
		}
	}(ctx)
}
