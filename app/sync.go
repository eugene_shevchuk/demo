package app

import (
	"context"

	"go.uber.org/zap"
)

func (app *App) Sync(ctx context.Context) {
	go func(ctx context.Context) {
		if !app.isSync {
			app.isSync = true
			app.logger.Info("sync starting...")
			for idx := int64(0); idx < 10000; idx++ {
				err := app.SyncPunk(ctx, idx)
				if err != nil {
					app.logger.Error("failed to sync punk", zap.Error(err), zap.Int64("idx", idx))
				}
			}
			app.isSync = false
			app.logger.Info("sync finished...")
		}
	}(ctx)
}

func (app *App) SyncPunk(ctx context.Context, idx int64) error {
	punk, err := app.GetPunk(idx)
	if err != nil {
		app.logger.Warn("failed to get punk by idx", zap.Error(err), zap.Int64("idx", idx))
		return err
	}
	err = app.UpdatePunk(ctx, punk)
	if err != nil {
		app.logger.Warn("failed to update punk", zap.Error(err), zap.Reflect("punk", punk))
		return err
	}
	return nil
}
