package config

import (
	"flag"
	"time"
)

var cfg Config

func init() {
	flag.StringVar(&cfg.Addr, "addr", ":8080", "App addr")
	flag.UintVar(&cfg.RetryAttempts, "retry-attempts", 10, "")
	flag.DurationVar(&cfg.RetryDelay, "retry-delay", 200, "millisecond")

	flag.StringVar(&cfg.DB.URI, "db-uri", "postgresql://postgres:123456@localhost:5432/crosspunks?sslmode=disable", "")
	flag.StringVar(&cfg.DB.MigrationTable, "migration-table", "schema_migrations", "")

	flag.StringVar(&cfg.Eth.RpcURL, "rpc-url", "https://bsc-dataseed.binance.org/", "")
	flag.StringVar(&cfg.Eth.NftAddress, "nft-addr", "0x360673A34cf5055DfC22C53bc063e948A243293B", "")
	flag.StringVar(&cfg.Eth.DexAddress, "dex-addr", "0x36894d06ac91B09760b4310C75Ed2348E3eA063C", "")

	flag.Parse()
}

type Config struct {
	Addr          string
	IsNeedSync    bool
	RetryAttempts uint
	RetryDelay    time.Duration

	DB  DB
	Eth Eth
}

type DB struct {
	URI            string
	MigrationTable string
}

type Eth struct {
	RpcURL     string
	NftAddress string
	DexAddress string
}

func Get() Config {
	return cfg
}
